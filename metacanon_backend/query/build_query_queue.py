import argparse
import logging

import numpy as np
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist

# noinspection PyUnresolvedReferences
import wsgi
from metacanon_backend.citations import query_types
from metacanon_backend.citations.query_types import QueryType

from metacanon_core.models import Work, Author
from metacanon_core.models.citation_count import CitationCount, WikipediaBacklinkCitationCount
from metacanon_core.models.query_queue import QueryQueue
from metacanon_core.query import get_query_args_for_type, NoDbpediaMetadataError

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger("django.db.backends").setLevel(logging.ERROR)
logger = logging.getLogger("build_query_queue")
logger.setLevel(logging.INFO)


def build_query_queue(query_type: str, force: bool):
    query_type_metadata: QueryType = query_types.get(query_type)

    if QueryQueue.objects.filter(query_type=query_type).count() > 0:
        logger.info(f"Query queue for type {query_type} already exists. Aborting.")
        return

    if query_type_metadata.related_type == Work:
        for work in Work.objects.all():
            if work.confirmation_status == "rejected":
                continue
            try:
                citation_count = query_type_metadata.citation_count_type.objects.get(work_id=work)
            except ObjectDoesNotExist:
                citation_count = None
            if force or citation_count is None or _decide_if_update(citation_count):
                try:
                    logger.info(f"Adding {work.work_id} to queue.")
                    query_args = get_query_args_for_type(query_type=query_type, work=work)
                except NoDbpediaMetadataError:
                    WikipediaBacklinkCitationCount(work_id=work, count=0)
                    continue
                QueryQueue(
                    work_id=work.work_id,
                    author_id=None,
                    query_type=query_type,
                    query_args=query_args,
                    force_query=force
                ).save()
    elif query_type_metadata.related_type == Author:
        for author in Author.objects.all():
            try:
                citation_count = query_type_metadata.citation_count_type.objects.get(author_id=author.author_id)
            except ObjectDoesNotExist:
                citation_count = None
            if force or citation_count is None or _decide_if_update(citation_count):
                logger.info(f"Adding {author.author_id} to queue.")
                query_args = get_query_args_for_type(query_type=query_type, author=author)
                QueryQueue(
                    work_id=None,
                    author_id=author.author_id,
                    query_type=query_type,
                    query_args=query_args,
                    force_query=force
                ).save()
    else:
        assert False


def _decide_if_update(citation_count: CitationCount) -> bool:
    try:
        diff: timedelta = citation_count.last_queried - citation_count.last_updated
    except TypeError:
        diff = timedelta(days=0)
    days_delta = diff.days if diff.days >= 1 else 1
    likelihood_of_update = 1 / days_delta
    return np.random.choice([True, False], p=[likelihood_of_update, 1 - likelihood_of_update])


def main():
    parser = argparse.ArgumentParser(description='Build citation count query queue.')
    parser.add_argument('--query-type', required=True)
    parser.add_argument(
        '--force', action="store_true", required=False, help="Query all works/author regardless of prior status."
    )
    args = parser.parse_args()
    build_query_queue(args.query_type, args.force)


if __name__ == "__main__":
    main()

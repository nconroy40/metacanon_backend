import argparse
import logging

import re

import dateparser

from datetime import date, datetime
from nameparser import HumanName
from pandas import DataFrame
from typing import Callable, Optional, List

# noinspection PyUnresolvedReferences
import wsgi

from time import sleep

import pandas as pd

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from metacanon_backend.citations import query_types
from metacanon_backend.citations.attempt_query import attempt_query, AttemptsState
from metacanon_backend.citations.citation_retrieval_error import CitationRetrievalError, AIDeclinedError
from metacanon_backend.citations.citation_count_updaters import update_citation_count
from metacanon_backend.citations.query_types import SourceAttributes, QueryType
from metacanon_backend.dbpedia.dbpedia_author_list_retriever import DbpediaAuthorListRetriever
from metacanon_backend.dbpedia.dbpedia_work_list_retriever import DbpediaWorkListRetriever
from settings import RL_AGENT_DATA_ROOT
from metacanon_core.models import Work, Author
from metacanon_core.models.dbpedia_metadata import DbpediaAuthorMetadata, DbpediaWorkMetadata
from metacanon_core.models.query_queue import QueryQueue

MAX = 10

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger("django.db.backends").setLevel(logging.ERROR)
logger = logging.getLogger("query_and_update")
logger.setLevel(logging.INFO)


def query_and_update(source: str):
    source_attributes = query_types.get_source_attributes(source)
    queue = _get_queue_for_source(source_attributes)
    if queue is None:
        return

    attempts_state = AttemptsState(
        RL_AGENT_DATA_ROOT,
        source,
        datetime.now(),
    )

    for i in range(source_attributes.limit):
        try:
            queue_entry: QueryQueue = queue[0]
        except IndexError:
            break

        logger.info(f"About to perform query for {queue_entry.query_type}.")
        logger.info(f"Retriever args: {queue_entry.query_args}.")

        query_type_metadata = query_types.get(queue_entry.query_type)

        _query(
            query_type_metadata=query_type_metadata,
            queue_entry=queue_entry,
            attempts_state=attempts_state
        )

        sleep(source_attributes.sleep_delay)

    attempts_state.save()


def _get_queue_for_source(source_attributes: SourceAttributes):
    queue = QueryQueue.objects.filter(query_type__in=source_attributes.query_types, status="ready")
    if queue.count() == 0:
        logger.warning("The queue is empty.")
        return None
    else:
        return queue


def _update_function_creator(
        query_type_metadata: QueryType,
        queue_entry: QueryQueue,
) -> Callable[[], None]:
    def update():
        retriever = query_type_metadata.retriever_class(verbose=True, **queue_entry.query_args)
        update_citation_count(
            citation_class=query_type_metadata.citation_count_type,
            citation_class_entry_id=queue_entry.get_entry_id(),
            get_num_citations_method=query_type_metadata.query_method,
            retriever=retriever,
            force=queue_entry.force_query
        )

    return update


def _query(
        query_type_metadata: QueryType,
        queue_entry: QueryQueue,
        attempts_state: AttemptsState
):
    update_function = _update_function_creator(query_type_metadata, queue_entry)
    try:
        if query_type_metadata.update_attempt_model_config is None:
            update_function()
        else:
            attempt_query(
                query_function=update_function,
                attempts_state=attempts_state,
                config=query_type_metadata.update_attempt_model_config
            )
        queue_entry.delete()
    except AIDeclinedError as e:
        logger.warning(e)
        logger.warning("Leaving entry in queue to be picked up on another run.")
    except CitationRetrievalError as e:
        logger.error(e)
        queue_entry.status = "failed"
        queue_entry.error = str(e)
        queue_entry.save()


def dbpedia_author_metadata():
    retriever = DbpediaAuthorListRetriever()
    author_metadata = retriever.run()
    df = pd.DataFrame(author_metadata)
    grouped: DataFrame = df.groupby(by=["resource"]).agg(list).reset_index()
    dictionaries = grouped.to_dict("records")

    def combine_name_elements(*elements):
        non_empty_elements = [e for e in elements if e]
        return " ".join(non_empty_elements)

    def parse_date(date_str: str) -> Optional[date]:
        if date_str is not None:
            try:
                return datetime.strptime(date_str, "%Y-%m-%d").date()
            except ValueError as e:
                logger.error(e)
                return None
        else:
            return None

    def first_non_null(items: List[str]):
        if items is None:
            return None

        for item in items:
            if item is not None:
                return item
        # Return None if nothing is found.
        return None

    for entry in dictionaries:
        label = entry.get("label")[0]
        name = HumanName(label)
        first_name = combine_name_elements(name.title, name.first, name.middle)
        last_name = combine_name_elements(name.last, name.suffix)
        alternate_names = entry.get("label")[1:-1]
        gender = first_non_null(entry.get("gender"))
        birth_date = parse_date(first_non_null(entry.get("birth_date")))
        death_date = parse_date(first_non_null(entry.get("death_date")))

        try:
            author = Author.objects.get(first_name=first_name, last_name=last_name)
        except ObjectDoesNotExist:
            author = Author(first_name=first_name, last_name=last_name)
        except MultipleObjectsReturned:
            author = Author.objects.get(first_name=first_name, last_name=last_name, date_of_birth=birth_date)

        author.author_gender = gender
        author.date_of_birth = birth_date
        author.date_of_death = death_date
        author.alternate_names = alternate_names

        print(f"First name: {first_name}.")
        print(f"Last name: {last_name}.")
        print(f"Gender: {gender}.")
        print(f"Birth date: {birth_date}.")
        print(f"Death date: {death_date}.")
        author.save()

        try:
            dbpedia = DbpediaAuthorMetadata.objects.get(author=author)
        except ObjectDoesNotExist:
            dbpedia = DbpediaAuthorMetadata(author=author)

        dbpedia.resource = entry.get("resource")
        dbpedia.gender = entry.get("gender")

        print(f"Dbpedia resource: {dbpedia.resource}")
        print("")

        dbpedia.save()


def dbpedia_work_metadata():
    def resolve_publication_year(dates: List[str]):
        years: List[int] = []
        for d in dates:
            if d is None:
                continue

            if re.match("^\\d{4}$", d) is not None:
                year = int(d)
                years.append(year)
            else:
                parsed_date = dateparser.parse(d)
                if parsed_date is None:
                    continue
                year = parsed_date.year
                years.append(year)
        if len(years) == 0:
            return None
        else:
            return min(years)

    retriever = DbpediaWorkListRetriever()
    works = retriever.run()
    df = pd.DataFrame(works)
    grouped: DataFrame = df.groupby(by=["author", "resource"]).agg(list).reset_index()
    grouped["pub_date"] = \
        grouped["release_date"] + \
        grouped["release_date_2"] + \
        grouped["published"] + \
        grouped["publication_date"] + \
        grouped["publication_date_2"]
    cleaned = grouped.drop(
        ["release_date", "release_date_2", "published", "publication_date", "publication_date_2"], axis=1
    )
    dictionary = cleaned.to_dict("records")
    for work in dictionary:
        pub_year = resolve_publication_year(work["pub_date"])
        titles = work["title"]
        author_resource = work["author"]
        work_resource = work["resource"]

        try:
            dbpedia_metadata = DbpediaAuthorMetadata.objects.get(resource=author_resource)
        except ObjectDoesNotExist:
            print(f"Found no author entry for {author_resource}.")
            continue
        except MultipleObjectsReturned:
            raise
        author = dbpedia_metadata.author

        try:
            work_dbpedia_metadata = DbpediaWorkMetadata.objects.get(resource=work_resource)
            work_in_db = work_dbpedia_metadata.work
        except ObjectDoesNotExist:
            works_by_author = Work.objects.filter(author_id=author.author_id)
            work_in_db = None
            for work_by_author in works_by_author:
                titles_in_db = [work_by_author.title] + work_by_author.alternate_titles
                if len(set(titles).intersection(titles_in_db)) > 0:
                    # At least one title is in common between the work in the db and the work in dbpedia
                    work_in_db = work_by_author

            if work_in_db is None:
                work_in_db = Work(title=titles[0], alternate_titles=titles[1:], author_id=author.author_id)
                work_in_db.save()

            work_dbpedia_metadata = DbpediaWorkMetadata(work=work_in_db, resource=work_resource,
                                                        publication_year=pub_year)
        except MultipleObjectsReturned:
            print(f"Work resource: {work_resource}.")
            raise

        if pub_year is not None and work_in_db.publication_year is None:
            # Update publication year of work if it is currently null.
            work_in_db.publication_year = pub_year

        work_in_db.save()
        work_dbpedia_metadata.save()


def main():
    parser = argparse.ArgumentParser(description='Build citation count query queue.')
    parser.add_argument('--source', required=True)
    args = parser.parse_args()

    if args.source == "dbpedia_work":
        dbpedia_work_metadata()
    elif args.source == "dbpedia_author":
        dbpedia_author_metadata()
    else:
        query_and_update(source=args.source)


if __name__ == "__main__":
    main()

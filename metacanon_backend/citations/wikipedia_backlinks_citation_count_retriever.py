import logging

import requests
import urllib


class WikipediaBacklinksCitationCountRetriever:
    def __init__(self, page_title: str, verbose: bool):
        self._page_title = page_title
        self._logger = logging.getLogger("WikipediaBacklinksCitationCountRetriever")
        self._logger.setLevel(logging.INFO)
        self._verbose = verbose

    def get_num_citations(self, blcontinue=None):
        request_url = "https://en.wikipedia.org/w/api.php"
        # noinspection PyUnresolvedReferences
        params = {
            "action": "query",
            "format": "json",
            "list": "backlinks",
            "bllimit": 500,
            "bltitle": urllib.parse.unquote_plus(self._page_title)
        }

        if blcontinue is not None:
            params["blcontinue"] = blcontinue

        if self._verbose:
            self._logger.info(f"Sending request to {request_url}")
            self._logger.info(f"Params: {params}")
        session = requests.Session()
        response = session.get(url=request_url, params=params).json()
        count = len(response["query"]["backlinks"])
        if "continue" in response:
            count = count + self.get_num_citations(blcontinue=response["continue"]["blcontinue"])

        return count


if __name__ == "__main__":
    print(WikipediaBacklinksCitationCountRetriever("Are_You_There_God%3F_It's_Me,_Margaret.", True).get_num_citations())

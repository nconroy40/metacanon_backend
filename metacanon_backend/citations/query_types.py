from django.db.models import Model
from typing import Type, Tuple, Optional, Union

from metacanon_backend.ai import rl_configs
from metacanon_backend.ai.reinforcement_learning_agent import ModelConfig
from metacanon_backend.citations.citations_count_retriever_base import CitationsCountRetrieverBase
from metacanon_backend.citations.hathi_trust_citations_count_retriever import HathiTrustCitationsCountRetriever
from metacanon_backend.citations.nyt_citations_count_retriever import NytCitationsCountRetriever
from metacanon_backend.citations.wikipedia_backlinks_citation_count_retriever import \
    WikipediaBacklinksCitationCountRetriever
from metacanon_core.models import Work, Author
from metacanon_core.models.citation_count import CitationCount, NewYorkTimesCitationCount, HathiTrustCitationCount, \
    HathiTrustTitleFrequency, HathiTrustAuthorFrequency, WikipediaBacklinkCitationCount


class QueryType:
    def __init__(
            self,
            retriever_class: Union[Type[CitationsCountRetrieverBase], Type[WikipediaBacklinksCitationCountRetriever]],
            query_method: str,
            related_type: Type[Model],
            citation_count_type: Type[CitationCount],
            update_attempt_model_config: Optional[ModelConfig]
    ):
        self.retriever_class: Type[CitationsCountRetrieverBase] = retriever_class
        self.query_method = query_method
        self.related_type = related_type
        self.citation_count_type = citation_count_type
        self.update_attempt_model_config = update_attempt_model_config


class SourceAttributes:
    def __init__(
            self,
            query_types: Tuple[str, ...],
            limit: int,
            sleep_delay: float,
    ):
        self.query_types = query_types
        self.limit = limit
        self.sleep_delay = sleep_delay


def get(query_type: str):
    query_type_metadata = {
        "nyt_citation_count": QueryType(
            retriever_class=NytCitationsCountRetriever,
            query_method="get_num_citations",
            related_type=Work,
            citation_count_type=NewYorkTimesCitationCount,
            update_attempt_model_config=None,
        ),
        "hathi_citation_count": QueryType(
            retriever_class=HathiTrustCitationsCountRetriever,
            query_method="get_num_citations",
            related_type=Work,
            citation_count_type=HathiTrustCitationCount,
            update_attempt_model_config=rl_configs.hathi()
        ),
        "hathi_title_frequency": QueryType(
            retriever_class=HathiTrustCitationsCountRetriever,
            query_method="get_title_frequency",
            related_type=Work,
            citation_count_type=HathiTrustTitleFrequency,
            update_attempt_model_config=rl_configs.hathi()
        ),
        "hathi_author_frequency": QueryType(
            retriever_class=HathiTrustCitationsCountRetriever,
            query_method="get_author_frequency",
            related_type=Author,
            citation_count_type=HathiTrustAuthorFrequency,
            update_attempt_model_config=rl_configs.hathi()
        ),
        "wikipedia_backlink_citation_count": QueryType(
            retriever_class=WikipediaBacklinksCitationCountRetriever,
            query_method="get_num_citations",
            related_type=Work,
            citation_count_type=WikipediaBacklinkCitationCount,
            update_attempt_model_config=None
        )
    }

    if query_type not in query_type_metadata:
        raise RuntimeError(f"Query type {query_type} is not supported.")
    else:
        return query_type_metadata[query_type]


def get_source_attributes(source: str) -> SourceAttributes:
    source_attributes = {
        "nyt": SourceAttributes(query_types=("nyt_citation_count",), limit=2, sleep_delay=6.0),
        "hathi": SourceAttributes(
            query_types=("hathi_citation_count", "hathi_title_frequency", "hathi_author_frequency"),
            limit=5,
            sleep_delay=1.0,
        ),
        "wikipedia": SourceAttributes(query_types=("wikipedia_backlink_citation_count",), limit=30, sleep_delay=1.0),
    }

    if source not in source_attributes:
        raise RuntimeError(f"Source {source} is not supported.")
    else:
        return source_attributes[source]

import argparse
import re

import bs4
import operator
import requests
from pprint import pprint

from typing import Dict, Optional, Tuple

from metacanon_backend.citations.citation_retrieval_error import UnusualTrafficError


class LocMetadataRetriever:
    """
    Retrieves work metadata from the Library of Congress website
    """

    def __init__(self,
                 author_first_name: str,
                 author_last_name: str,
                 title: str):
        self._author_first_name = author_first_name
        self._author_last_name = author_last_name
        self._title = title

    @property
    def url(self):
        return f"https://catalog.loc.gov/vwebv/search?searchArg1={self._title}&argType1=all&searchCode1=KTIL&" \
               f"searchType=2&combine2=and&searchArg2={self._author_first_name} {self._author_last_name}&" \
               f"argType2=all&searchCode2=KPNC&combine3=and&searchArg3=&argType3=all&searchCode3=GKEY&year=1520-3000&" \
               f"fromYear=&toYear=&location=all&place=all&type=am&language=all&recCount=100"

    def run(self) -> Dict:
        user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
        accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        accept_language = "en-US,en;q=0.5"
        accept_encoding = "gzip, deflate, br"
        headers = {'user-agent': user_agent,
                   'accept': accept,
                   'accept-language': accept_language,
                   'accept-encoding': accept_encoding,
                   'referer': "https://www.google.com/"}
        raw_html = requests.get(self.url, headers=headers)
        soup = bs4.BeautifulSoup(raw_html.text, "html.parser")

        loc_metadata = {
            "publication_year": None,
            "loc_title": None,
            "loc_date": None,
            "loc_author": None,
            "loc_format": None
        }
        if "search found no results" in soup.text:
            return loc_metadata

        if "available connections to the LC Catalog are currently in use" in soup.text:
            raise UnusualTrafficError("available connections to the LC Catalog are currently in use")

        def get_year(work) -> Tuple[Optional[int], object]:
            date_string = work.find("div", {"class": "search-results-list-description-date"})
            try:
                year = year_from_loc_date(date_string.text)
            except AttributeError:
                year = None
            if year is not None:
                return int(year), work
            else:
                return None, work

        def is_valid(year, work):
            if year is None:
                return False
            else:
                loc_author = work.find("div", {"class": "search-results-list-description-name"})
                return loc_author is not None and self._author_last_name.encode(
                    "utf-8").lower() in loc_author.text.lower().encode("utf-8")

        works = {year: work for year, work in
                 map(get_year, soup.find_all("div", {"class": "search-results-list-description"}))
                 if is_valid(year, work)}

        try:
            work = min(works.items(), key=operator.itemgetter(0))
            loc_metadata = {
                "publication_year": work[0],
                "loc_title": work[1].find("div", {"class": "search-results-list-description-title"}).text.encode(
                    "utf-8"),
                "loc_date": work[1].find("div", {"class": "search-results-list-description-date"}).text.encode("utf-8"),
                "loc_author": work[1].find("div", {"class": "search-results-list-description-name"}).text.encode(
                    "utf-8"),
                "loc_format": work[1].find("div", {"class": "search-results-list-description-format"}).text.encode(
                    "utf-8")
            }
        except ValueError:
            # There is only one result, so we've been taken directly to the entry for this work.
            work = soup.find("div", {"class": "top-information-content"})
            if work is None:
                print(soup.encode("utf-8"))
            else:
                item_titles = [t.text for t in work.find_all("h3", {'item-title'})]
                item_descriptions = [d.text for d in work.find_all("ul", {"item-description"})]
                assert len(item_titles) > 0
                for i in range(0, len(item_titles)):
                    if "personal name" in item_titles[i].lower():
                        loc_metadata["loc_author"] = item_descriptions[i]
                    elif "main title" in item_titles[i].lower():
                        loc_metadata["loc_title"] = item_descriptions[i]
                    elif "published" in item_titles[i].lower():
                        loc_metadata["loc_date"] = item_descriptions[i]

            try:
                loc_metadata["publication_year"] = int(re.search(r"(\d{4})", loc_metadata["loc_date"]).group(1))
            except (AttributeError, TypeError):
                loc_metadata["publication_year"] = None

        return loc_metadata


def year_from_loc_date(loc_date: str) -> str:
    match = re.search(r"\[(\d{4})\]", loc_date)
    if match is None:
        match = re.search(r"[p](\d{4})", loc_date)

    if match is None:
        match = re.search(r"(\d{4}), c\d{4}", loc_date)

    if match is None:
        matches = re.findall(r"(^|\D)(\d{4})($|\D)", loc_date)
        if len(matches) > 0:
            match = matches[len(matches) - 1]
            year = match[1]
        else:
            year = None
    else:
        year = match.group(1)

    return year


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get Library of Congress Metadata')
    parser.add_argument('--first-name', nargs='?', required=True)
    parser.add_argument('--last-name', nargs='?', required=True)
    parser.add_argument('--title', nargs='?', required=True)
    args = parser.parse_args()

    retriever = LocMetadataRetriever(
        author_first_name=args.first_name,
        author_last_name=args.last_name,
        title=args.title
    )

    try:
        metadata = retriever.run()
        print(retriever.url)
        pprint(metadata)
    except Exception as e:
        print(retriever.url)
        raise

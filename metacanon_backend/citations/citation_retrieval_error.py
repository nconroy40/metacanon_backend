
class CitationRetrievalError(Exception):
    pass


class IllegalZeroCountError(CitationRetrievalError):
    """
    Count was zero when it shouldn't have been. This might indicate that we are
    getting blocked by the source.
    """
    pass


class CountChangedTooMuchError(CitationRetrievalError):
    """
    We received a valid response from the source, but the count changed more than expected.
    """
    pass


class UnusualTrafficError(CitationRetrievalError):
    """
    We are getting explicitly blocked by the source.
    """
    pass


class UnknownCitationRetrievalError(CitationRetrievalError):
    """
    Something went wrong, but we don't know what.
    """
    pass


class AIDeclinedError(CitationRetrievalError):
    """
    Our AI decided not to query.
    """
    pass

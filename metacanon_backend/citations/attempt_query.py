import logging
import os

import json

import ast
from datetime import datetime

from datetime import timedelta
from pandas import DataFrame
import pandas as pd
from typing import Callable, Optional, Dict, Union

from metacanon_backend.ai.reinforcement_learning_agent import Model, ModelConfig
from metacanon_backend.citations.citation_retrieval_error import UnusualTrafficError, AIDeclinedError, \
    CountChangedTooMuchError


logger = logging.getLogger("attempt_query")
logger.setLevel(logging.INFO)


class AttemptsStateEntry:
    def __init__(
            self,
            timestamp: Optional[datetime],
            attempted: bool,
            blocked: bool,
            seconds_since_last: float,
            last_was_successful: bool,
            num_recent_queries: int,
    ):
        self.timestamp = timestamp
        self.attempted = attempted
        self.blocked = blocked  # True if we tried to query and were denied
        self.seconds_since_last = seconds_since_last
        self.last_was_successful = last_was_successful  # True if the most recent previous attempt was successful
        self.num_recent_queries = num_recent_queries  # Number of recent attempted queries

    def __eq__(self, other) -> bool:
        return self.__dict__ == other.__dict__


class AttemptsState:
    def __init__(self, path: str, query_type: str, now: datetime):
        self._data_frame_columns = [
            'timestamp',
            'attempted',
            'blocked',
            'seconds_since_last',
            'last_was_successful',
            'num_recent_queries'
        ]
        self._path = os.path.join(path, query_type)
        if not os.path.exists(self._path):
            os.makedirs(self._path)
        self._entries: DataFrame = self._read_entries(now)
        self._recent_entries: DataFrame = self._filter_entries_to_recent(self._entries, now)
        self._weights: Dict = self._read_weights()

    @property
    def last_attempt(self) -> Optional[AttemptsStateEntry]:
        if self._entries.empty:
            return None
        else:
            return self._encode(self._latest(self._entries[self._entries["attempted"]]))

    @property
    def last_recorded(self) -> Optional[AttemptsStateEntry]:
        if self._entries.empty:
            return None
        else:
            return self._encode(self._latest(self._entries))

    @property
    def num_recent_queries(self) -> int:
        attempted = self._recent_entries["attempted"]
        return len(self._recent_entries[attempted].index)

    @property
    def weights(self) -> Dict:
        return self._weights

    def update(
            self,
            timestamp: datetime,
            attempted: Union[int, bool],
            blocked: bool,
            rl_model: Dict,
            seconds_since_last: int,
            last_was_successful: bool,
            num_recent_queries: int):
        self._weights = rl_model

        new_entry = DataFrame(
            [[timestamp, bool(attempted), blocked, seconds_since_last, last_was_successful, num_recent_queries]],
            columns=self._data_frame_columns
        )

        self._entries = self._filter_entries_to_recent(
            pd.concat([self._entries, new_entry], ignore_index=True, sort=False), timestamp
        )
        self._recent_entries: DataFrame = self._filter_entries_to_recent(self._entries, timestamp)

    def save(self):
        with open(self._model_path, "w") as f:
            json.dump({str(k): v for k, v in self._weights.items()}, f)

        self._entries.to_pickle(self._entries_path)

    @property
    def _model_path(self):
        return os.path.join(self._path, "weights.json")

    @property
    def _entries_path(self):
        return os.path.join(self._path, "entries.p")

    @staticmethod
    def _latest(entries: DataFrame) -> pd.Series:
        return entries.loc[entries["timestamp"].idxmax()]

    @staticmethod
    def _encode(entry: pd.Series) -> AttemptsStateEntry:
        return AttemptsStateEntry(
            timestamp=entry["timestamp"],
            attempted=entry["attempted"],
            blocked=entry["blocked"],
            seconds_since_last=entry["seconds_since_last"],
            last_was_successful=entry["last_was_successful"],
            num_recent_queries=entry["num_recent_queries"]
        )

    def _read_entries(self, now: datetime) -> DataFrame:
        if os.path.exists(self._entries_path):
            entries = pd.read_pickle(self._entries_path)
            return self._filter_entries_to_available(entries, now)
        else:
            return pd.DataFrame(columns=self._data_frame_columns)

    @staticmethod
    def _filter_entries_to_recent(entries: DataFrame, now: datetime):
        return entries[entries["timestamp"] > now - timedelta(hours=12)]

    @staticmethod
    def _filter_entries_to_available(entries: DataFrame, now: datetime):
        return entries[entries["timestamp"] > now - timedelta(hours=48)]

    def _read_weights(self) -> Dict:
        path = self._model_path
        if os.path.exists(path):
            with open(path) as p:
                try:
                    data = json.load(p)
                except ValueError:
                    data = ast.literal_eval(p)
                return {eval(k): v for k, v in data.items()}
        else:
            return {}


def attempt_query(
        query_function: Callable[[], None],
        attempts_state: AttemptsState,
        config: ModelConfig
):
    now: datetime = datetime.now()
    num_recent_queries = attempts_state.num_recent_queries

    last_attempt = attempts_state.last_attempt
    if last_attempt is None:
        # Max for case when there is no recent query
        seconds_since_last = 12 * 60 * 60
        last_was_successful = True
    else:
        seconds_since_last = int((now - last_attempt.timestamp).total_seconds())
        last_was_successful = not last_attempt.blocked

    last_recorded = attempts_state.last_recorded
    if last_recorded is None:
        last_recorded = AttemptsStateEntry(
            seconds_since_last=seconds_since_last,
            last_was_successful=True,
            num_recent_queries=0,
            attempted=True,
            blocked=False,
            timestamp=None
        )

    weights = attempts_state.weights

    model = Model(
        weights=weights,
        config=config
    )

    should_query = model.get_decision(
        seconds_since_last=seconds_since_last,
        last_was_successful=last_was_successful,
        num_recent_queries=num_recent_queries)

    this_state = model.encode_state(seconds_since_last, last_was_successful, num_recent_queries)

    previous_state = model.encode_state(
        last_recorded.seconds_since_last,
        last_recorded.last_was_successful,
        last_recorded.num_recent_queries)
    previous_action = last_recorded.attempted

    if should_query:
        try:
            return_value = query_function()
            error = None
            blocked = False
        except CountChangedTooMuchError as e:
            return_value = None
            error = e
            blocked = False
        except UnusualTrafficError as e:
            return_value = None
            error = e
            blocked = True
    else:
        return_value = None
        message = "Decided not to query."
        error = AIDeclinedError(message)
        blocked = False

    model.update(previous_state, this_state, int(previous_action), int(should_query))
    attempts_state.update(
        timestamp=now,
        attempted=should_query,
        blocked=blocked,
        seconds_since_last=seconds_since_last,
        last_was_successful=last_was_successful,
        num_recent_queries=num_recent_queries,
        rl_model=model.q
    )

    if error is not None:
        raise error

    return return_value

import urllib
import random

import bs4
import re
import string

from metacanon_backend.citations.citation_retrieval_error import CitationRetrievalError, UnusualTrafficError
from metacanon_backend.citations.citations_count_retriever_base import ScrapingCitationsCountRetriever


class JstorCitationsCountRetriever(ScrapingCitationsCountRetriever):

    def get_num_citations(self) -> int:
        url = self.generate_url()
        return self._get_num_citations(url)

    def get_num_lit_citations(self):
        url = self.generate_lit_url()
        return self._get_num_citations(url)

    def get_num_alh_citations(self):
        url = self.generate_alh_url()
        return self._get_num_citations(url)

    def get_num_al_citations(self):
        url = self.generate_al_url()
        return self._get_num_citations(url)

    @staticmethod
    def _get_num_citations_from_raw_html(raw_html):
        soup = bs4.BeautifulSoup(raw_html.text, "html.parser")
        if soup.find("html") is None:
            print("Couldn't parse html file")
            print(soup)
            raise CitationRetrievalError("Hit a fatal error while attempting to retrieve citation counts. "
                                         "No html element.")
        else:
            results_element = soup.find("h2", {"data-result-count": True})
            if results_element is not None:
                # noinspection PyUnresolvedReferences
                results_string = results_element["data-result-count"]
                try:
                    total_results = int(re.sub('[^0-9]', '', results_string))
                except ValueError:
                    raise CitationRetrievalError("Failed to convert results string to integer.")
            elif soup.find("h2", text=re.compile(r'No results')):
                total_results = 0
            elif "unusual traffic activity" in raw_html.text:
                raise UnusualTrafficError("The JSTOR server is intentionally blocking our traffic. Try again later.")
            else:
                print("Couldn't find results in html file")
                try:
                    print(raw_html.text)
                except UnicodeEncodeError as e:
                    print(e)
                raise UnusualTrafficError(
                    "Hit a fatal error while attempting to retrieve citation counts. No results element in response.")

        return total_results

    def generate_url(self):
        search_term = JstorCitationsCountRetriever._get_search_term(
            self._author_first_name, self._author_last_name, self._titles
        )
        # noinspection PyUnresolvedReferences
        url = "https://www.jstor.org/action/doBasicSearch?Query=" + urllib.parse.quote(search_term, safe='') + \
              "&acc=off&wc=on&fc=off&group=none"
        return url

    def generate_lit_url(self):
        search_term = JstorCitationsCountRetriever._get_search_term(
            self._author_first_name, self._author_last_name, self._titles
        )
        # noinspection PyUnresolvedReferences
        url = "https://www.jstor.org/action/doAdvancedSearch?isbn=&f4=all&c6=AND&c2=AND&f3=all&f0=all&la=&sd=&group=" \
              "none&q5=&dc.literature-discipline=on&q3=&q4=&c3=AND&acc=off&pt=&f5=all&q1=&c1=AND&f2=all&q6=&f6=" \
              "all&q0=" + urllib.parse.quote(search_term, safe='') + "&ed=&c5=AND&f1=all&q2=&c4=AND"
        return url

    def generate_alh_url(self):
        search_term = JstorCitationsCountRetriever._get_search_term(
            self._author_first_name, self._author_last_name, self._titles
        )
        # noinspection PyUnresolvedReferences
        url = "https://www.jstor.org/action/doAdvancedSearch?sd=&isbn=&acc=off&q5=&q4=&group=none&ed=&la=&f6=all&q0=" \
              + urllib.parse.quote(search_term, safe='') + "&q1=&q3=&f3=all&q2=&q6=&c4=AND&f0=all&f5=all&c2=AND&c6=" \
              "AND&c1=AND&pt=&f1=all&f2=all&f4=all&jc.AmericanLiteraryHistory=j100807&c5=AND&c3=AND"
        return url

    def generate_al_url(self):
        search_term = JstorCitationsCountRetriever._get_search_term(
            self._author_first_name, self._author_last_name, self._titles
        )
        # noinspection PyUnresolvedReferences
        url = "https://www.jstor.org/action/doAdvancedSearch?c2=AND&c4=AND&q6=&jc.AmericanLiterature=j100068&group=" \
              "none&f3=all&q3=&q0=" + urllib.parse.quote(search_term, safe='') + "&f4=all&sd=&q4=&f1=all&f2=all&acc=" \
              "off&q2=&q1=&c1=AND&f0=all&c3=AND&q5=&la=&f5=all&ed=&isbn=&pt=&c5=AND&f6=all&c6=AND"
        return url

    @staticmethod
    def _get_random_string(length):
        random_string = ""
        for i in range(0, length):
            random_string += random.choice(string.ascii_lowercase)
        return random_string

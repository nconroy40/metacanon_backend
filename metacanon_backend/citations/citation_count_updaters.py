import logging

from django.utils import timezone
from typing import Type

from metacanon_backend.citations.citation_retrieval_error import CountChangedTooMuchError, IllegalZeroCountError
from metacanon_backend.citations.citations_count_retriever_base import CitationsCountRetrieverBase
from metacanon_core.models.citation_count import CitationCount

UPDATE_TIME_THRESHOLD = 100
MAX_NEW_COUNT_FACTOR = 3


logger = logging.getLogger("citation_count_updaters")
logger.setLevel(logging.INFO)


def update_citation_count(
        citation_class: Type[CitationCount],
        citation_class_entry_id: int,
        get_num_citations_method: str,
        retriever: CitationsCountRetrieverBase,
        force: bool,
) -> None:
    """
    Return true if a query was made, regardless of whether or not it was successful.
    :param retriever:
    :param citation_class_entry_id:
    :param citation_class:
    :param get_num_citations_method:
    :param force: update even if there is a dramatic drop in the citation count
    :return:
    """

    if citation_class.exists(citation_class_entry_id):
        citation_count_entry = citation_class.get_by_id(citation_class_entry_id)
        old_citation_count = citation_count_entry.count
    else:
        old_citation_count = None

    _perform_update(
        get_num_citations_method,
        retriever,
        old_citation_count,
        citation_class,
        citation_class_entry_id,
        force
    )


def _perform_update(
        get_num_citations_method: str,
        retriever: CitationsCountRetrieverBase,
        old_citation_count: int,
        citation_class: Type[CitationCount],
        citation_class_entry_id: int,
        force: bool = False,
):
    new_citation_count = getattr(retriever, get_num_citations_method)()
    if old_citation_count is not None and old_citation_count > 20 and not force:
        _assert_new_count_non_zero(old_citation_count, new_citation_count)
        _assert_count_change_in_acceptable_range(old_citation_count, new_citation_count)
    logger.info(f"Old citation count: {old_citation_count}")
    logger.info(f"New citation count: {new_citation_count}")
    logger.info(f"Updating {citation_class.name()} in database.")
    citation_class.update_or_insert(citation_class_entry_id, new_citation_count, timezone.now())


def _assert_count_change_in_acceptable_range(old_count, new_count):
    def new_too_small():
        try:
            return old_count > new_count * MAX_NEW_COUNT_FACTOR
        except TypeError as e:
            logger.info(f"Old count: {old_count}.")
            logger.info(f"New count: {new_count}.")
            logger.info(f"Max new count factor: {MAX_NEW_COUNT_FACTOR}.")
            raise e

    if new_too_small():
        message = f"Cannot update because difference between old and new counts is too great. " \
                  f"Old_count: {old_count}, New count: {new_count}"
        raise CountChangedTooMuchError(message)


def _assert_new_count_non_zero(old_citation_count, new_citation_count):
    if new_citation_count == 0:
        # This probably means that something is seriously wrong, we should stop attempting to collection citations.
        message = f"Old citation count was {old_citation_count} but new citation count was zero."
        raise IllegalZeroCountError(message)

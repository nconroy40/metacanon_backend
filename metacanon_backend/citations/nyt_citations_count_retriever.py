import requests
import urllib

from metacanon_backend.citations.citation_retrieval_error import UnknownCitationRetrievalError
from metacanon_backend.citations.citations_count_retriever_base import CitationsCountRetrieverBase


class NytCitationsCountRetriever(CitationsCountRetrieverBase):
    _base_uri = "https://api.nytimes.com/svc/search/v2/articlesearch.json"
    _api_key = "tacO8rxNCIb8c8eDGDuXAcLtMvWMiF3V"

    def get_num_citations(self):
        search_term = self._get_search_term(self._author_first_name, self._author_last_name, self._titles)
        # noinspection PyUnresolvedReferences
        request_url = f"{self._base_uri}?fq={urllib.parse.quote_plus(search_term)}&api-key={self._api_key}"
        self._logger.info(f"Sending request to {request_url}")
        response = requests.get(request_url).json()
        if response["status"] == "ERROR":
            raise UnknownCitationRetrievalError("Response from nyt api was ERROR.")
        return int(response['response']['meta']['hits'])

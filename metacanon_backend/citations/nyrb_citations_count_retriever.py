# import re
# import urllib
#
# import requests
#
# from metacanon_09.citations.citation_retrieval_error import UnusualTrafficError
# from metacanon_09.citations.citations_count_retriever_base import CitationsCountRetrieverBase
#
#
# # noinspection DuplicatedCode
# class NyrbCitationsCountRetriever(CitationsCountRetrieverBase):
#     def generate_url(self):
#         search_term = self._get_search_term(self._author_first_name, self._author_last_name, self._titles)
#         # noinspection PyUnresolvedReferences
#         url = "https://www.nybooks.com/search/?s=" + urllib.parse.quote(search_term, safe='')
#         print(url)
#         return url
#
#     def get_num_citations(self):
#         url: str = self.generate_url()
#         if self._verbose:
#             print("Sending request to %s" % url)
#         user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
#         accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
#         accept_language = "en-US,en;q=0.5"
#         # accept_encoding = "gzip, deflate, br"
#         headers = {'user-agent': user_agent,
#                    'accept': accept,
#                    'accept-language': accept_language,
#                    # 'accept-encoding': accept_encoding,
#                    'referer': "https://www.google.com/"}
#         if self._session is not None:
#             self._session.headers.update(headers)
#             response = self._session.get(url)
#             if self._verbose:
#                 print("Using headers:")
#                 print(self._session.headers)
#         else:
#             response = requests.get(url, headers=headers)
#         response.encoding = response.apparent_encoding
#         raw_html = response.text
#         return self._get_num_citations_from_raw_html(raw_html)
#
#     @staticmethod
#     def _get_num_citations_from_raw_html(raw_html: str) -> int:
#         match = re.search(r"All {2}\((\d*)\)", raw_html)
#         if match:
#             return int(match.group(1))
#         elif re.search("search did not match any documents", raw_html):
#             return 0
#         else:
#             print(raw_html.encode("utf-8"))
#             raise UnusualTrafficError

import datetime

from metacanon_backend.ai import rl_configs
from metacanon_backend.citations.attempt_query import attempt_query, AttemptsState
from metacanon_backend.scholar.scholar import ScholarQuerier, SearchScholarQuery, ScholarConf, \
    ClusterScholarQuery
from metacanon_backend.citations.util import is_same_title
from settings import RL_AGENT_DATA_ROOT


class QueryManipulator:
    def __init__(self, title, articles):
        self.title = title
        self.articles = articles

    def find_best_matching_article(self):
        num_citations = None
        best_matching_article = None
        for art in self.articles:
            if is_same_title(self.title, art.attrs['title'][0]):
                new_num_citations = art.attrs['num_citations'][0]
                if num_citations is None or new_num_citations > num_citations:
                    num_citations = new_num_citations
                    best_matching_article = art

        return best_matching_article

    def generate_report(self):
        best_matching_article = self.find_best_matching_article()

        articles_list = []
        for article in self.articles:
            entry = {'title': article.attrs['title'][0],
                     'num_citations': article.attrs['num_citations'][0],
                     'cluster_id': article.attrs['cluster_id'][0]}
            articles_list.append(entry)

        output = {'best_match_title': best_matching_article.attrs['title'][0] if best_matching_article else None,
                  'best_match_num_citations': best_matching_article.attrs['num_citations'][
                      0] if best_matching_article else None,
                  'best_match_cluster_id':
                      best_matching_article.attrs['cluster_id'][0] if best_matching_article else None,
                  'articles_list': articles_list,
                  }
        return output

    def get_num_citations(self):
        best_matching_article = QueryManipulator(self.title, self.articles).find_best_matching_article()

        if not best_matching_article:
            num_citations = "Work not found"
        else:
            num_citations = best_matching_article.attrs['num_citations'][0]

        return num_citations


class GoogleScholarCitationsCountRetriever:
    def __init__(self, author_first_name: str, author_last_name: str, title: str, cluster_id: int = None):
        self._author_first_name = author_first_name
        self._author_last_name = author_last_name
        self._title = title
        self._cluster_id = cluster_id
        self._gs_report = None

    # noinspection PyUnusedLocal
    @staticmethod
    def _retrieve_articles(author_first_name, author_last_name, title):
        querier = ScholarQuerier()
        query = SearchScholarQuery()
        # ignoring first name for now
        query.set_words(f"{author_last_name} {title}")
        query.set_num_page_results(min(10, ScholarConf.MAX_PAGE_RESULTS))

        def send_query():
            querier.send_query(query)

        attempts_state = AttemptsState(
            RL_AGENT_DATA_ROOT,
            "google_scholar",
            datetime.datetime.now(),
        )

        attempt_query(
            query_function=send_query,
            attempts_state=attempts_state,
            config=rl_configs.google_scholar()
        )

        return querier.articles

    @property
    def cluster_id(self) -> int:
        return self._cluster_id

    @property
    def gs_report(self):
        if self._gs_report is None:
            return {'best_match_num_citations': None, 'best_match_cluster_id': None}
        else:
            return self._gs_report

    def url(self) -> str:
        query = SearchScholarQuery()
        query.set_words(f"{self._author_last_name} {self._title}")
        query.set_num_page_results(min(10, ScholarConf.MAX_PAGE_RESULTS))
        return query.get_url()

    def get_num_citations(self) -> int:
        articles = self._retrieve_articles(self._author_first_name, self._author_last_name, self._title)
        return QueryManipulator(self._title, articles).get_num_citations()

    def get_num_citations_by_cluster_id(self) -> int:
        if self._cluster_id is None:
            raise ValueError("Cannot get num citations by cluster id if cluster id is null.")
        else:
            querier = ScholarQuerier()
            query = ClusterScholarQuery(cluster=self._cluster_id)
            query.set_num_page_results(min(10, ScholarConf.MAX_PAGE_RESULTS))
            querier.send_query(query)
            return querier.articles[0].attrs['num_citations'][0]

    def get_google_scholar_report(self):
        if self._gs_report is None:
            articles = self._retrieve_articles(self._author_first_name, self._author_last_name, self._title)
            self._gs_report = QueryManipulator(self._title, articles).generate_report()
        return self._gs_report

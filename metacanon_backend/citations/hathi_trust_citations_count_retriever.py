import bs4
import re

import urllib
from typing import List

from metacanon_backend.citations.citation_retrieval_error import UnusualTrafficError, CitationRetrievalError
from metacanon_backend.citations.citations_count_retriever_base import ScrapingCitationsCountRetriever


class HathiTrustCitationsCountRetriever(ScrapingCitationsCountRetriever):
    def __init__(self,
                 author_first_name: str,
                 author_last_name: str,
                 titles: List[str] = None,
                 verbose=False,
                 session=None,
                 publication_year=None):
        super().__init__(author_first_name, author_last_name, titles, verbose, session)
        self._publication_year = publication_year

    def get_num_citations(self) -> int:
        url = self.generate_url()
        return self._get_num_citations(url)

    def get_author_frequency(self) -> int:
        url = self.generate_author_frequency_url()
        return self._get_num_citations(url)

    def get_title_frequency(self) -> int:
        url = self.generate_title_frequency_url()
        return self._get_num_citations(url)

    def generate_url(self) -> str:
        # noinspection PyUnresolvedReferences
        clean_first_name = urllib.parse.quote_plus(self._author_first_name)
        # noinspection PyUnresolvedReferences
        clean_last_name = urllib.parse.quote_plus(self._author_last_name)
        author_phrase = f'"{clean_first_name}%20{clean_last_name}"%20OR%20"{clean_last_name}%2C%20{clean_first_name}"'

        # noinspection PyUnresolvedReferences
        clean_titles = [f'"{urllib.parse.quote_plus(t.replace("?", "").replace("!", ""))}"' for t in self._titles]
        # noinspection PyUnresolvedReferences
        title_phrase = '%20OR%20'.join(clean_titles)

        url = \
            f"https://babel.hathitrust.org/cgi/ls?a=srchls" \
            f"&q1={author_phrase}" \
            f"&field1=ocr" \
            f"&anyall1=phrase" \
            f"&q2={title_phrase}" \
            f"&field2=ocr" \
            f"&anyall2=phrase" \
            f"&op2=AND" \
            f"&q3={self._subjects}" \
            f"&field3=subject" \
            f"&anyall3=all" \
            f"&op3=AND" \
            f"&yop=after"

        if self._publication_year is not None:
            url += f"&pdate_start={self._publication_year}"

        return url

    def generate_author_frequency_url(self) -> str:
        # noinspection PyUnresolvedReferences
        clean_first_name = urllib.parse.quote_plus(self._author_first_name)
        # noinspection PyUnresolvedReferences
        clean_last_name = urllib.parse.quote_plus(self._author_last_name)
        author_phrase = f'"{clean_first_name}%20{clean_last_name}"%20OR%20"{clean_last_name}%2C%20{clean_first_name}"'

        url = \
            f"https://babel.hathitrust.org/cgi/ls?a=srchls" \
            f"&q1={author_phrase}" \
            f"&field1=ocr" \
            f"&anyall1=phrase" \
            f"&op2=AND" \
            f"&q3={self._subjects}" \
            f"&field3=subject" \
            f"&anyall3=all" \
            f"&op3=AND"

        return url

    def generate_title_frequency_url(self) -> str:
        # noinspection PyUnresolvedReferences
        clean_titles = [f'"{urllib.parse.quote_plus(t.replace("?", "").replace("!", ""))}"' for t in self._titles]
        # noinspection PyUnresolvedReferences
        title_phrase = '%20OR%20'.join(clean_titles)

        url = \
            f"https://babel.hathitrust.org/cgi/ls?a=srchls" \
            f"&q2={title_phrase}" \
            f"&field2=ocr" \
            f"&anyall2=phrase" \
            f"&op2=AND" \
            f"&q3={self._subjects}" \
            f"&field3=subject" \
            f"&anyall3=all" \
            f"&op3=AND" \
            f"&yop=after"

        if self._publication_year is not None:
            url += f"&pdate_start={self._publication_year}"

        return url

    @property
    def _subjects(self):
        # noinspection PyUnresolvedReferences
        return urllib.parse.quote_plus(
            '"Literature" OR "Social sciences" OR "Political science" OR "Humanities" OR "History"'
        )

    @staticmethod
    def _get_num_citations_from_raw_html(raw_html):
        soup = bs4.BeautifulSoup(raw_html.text, "html.parser")

        results_element = soup.find("h2", {"class": "results-summary"})
        if results_element is not None:
            # noinspection PyUnresolvedReferences
            results_string = results_element.text
            if "No results" in results_string:
                total_results = 0
            else:
                try:
                    fixed = re.sub("1 to [\\d]* of ", "", results_string)
                    fixed = re.sub(" Full-Text results", "", fixed)
                    fixed = re.sub(",", "", fixed)
                    total_results = int(fixed)
                except ValueError:
                    raise CitationRetrievalError("Failed to convert results string to integer.")
        else:
            print("Couldn't find results in html file")
            try:
                print(raw_html.text)
            except UnicodeEncodeError as e:
                print(e)
            raise UnusualTrafficError(
                "Hit a fatal error while attempting to retrieve citation counts. No results element in response.")

        return total_results

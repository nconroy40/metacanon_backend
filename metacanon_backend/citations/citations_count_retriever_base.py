import logging

import requests
from typing import List

from abc import abstractmethod, ABCMeta

from metacanon_backend.citations.citation_retrieval_error import UnusualTrafficError


class CitationsCountRetrieverBase(metaclass=ABCMeta):
    def __init__(self,
                 author_first_name: str,
                 author_last_name: str,
                 titles: List[str] = None,
                 verbose=False,
                 session=None):
        self._author_first_name = author_first_name
        self._author_last_name = author_last_name
        self._titles = titles
        self._verbose = verbose
        self._session = session
        self._logger = logging.getLogger("CitationsCountRetrieverBase")
        self._logger.setLevel(logging.INFO)

    @abstractmethod
    def get_num_citations(self):
        pass

    @staticmethod
    def _get_search_term(author_first_name: str, author_last_name: str, titles: List[str]) -> str:
        normalized_first_name = normalize_string(author_first_name)
        normalized_last_name = normalize_string(author_last_name)
        titles_term = ''
        for i in range(0, len(titles)):
            titles_term += '"%s"' % normalize_string(titles[i])
            if i != len(titles) - 1:
                titles_term += ' OR '
        return '(("%s %s") OR ("%s, %s")) AND (%s)' % (
            normalized_first_name,
            normalized_last_name,
            normalized_last_name,
            normalized_first_name,
            titles_term
        )


class ScrapingCitationsCountRetriever(CitationsCountRetrieverBase, metaclass=ABCMeta):
    def _get_num_citations(self, url: str) -> int:
        if self._verbose:
            try:
                self._logger.info(f"Sending request to {url}.")
            except UnicodeEncodeError as e:
                self._logger.error(e)

        user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0"
        accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        accept_language = "en-US,en;q=0.5"
        accept_encoding = "gzip, deflate, br"
        headers = {'user-agent': user_agent,
                   'accept': accept,
                   'accept-language': accept_language,
                   'accept-encoding': accept_encoding,
                   'referer': "https://www.google.com/"}
        if self._session is not None:
            self._session.headers.update(headers)
            raw_html = self._session.get(url)
            if self._verbose:
                self._logger.info("Using headers:")
                self._logger.info(self._session.headers)
        else:
            try:
                raw_html = requests.get(url, headers=headers)
            except requests.exceptions.ConnectionError:
                raise UnusualTrafficError("Hit a connection error when attempting to connect to resource.")
        return self._get_num_citations_from_raw_html(raw_html)

    @staticmethod
    @abstractmethod
    def _get_num_citations_from_raw_html(raw_html):
        pass


def normalize_string(string: str):
    return string.replace("&oslash;", "o").replace("&Oslash;", "O")

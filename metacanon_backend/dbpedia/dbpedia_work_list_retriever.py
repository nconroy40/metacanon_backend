from typing import List, Dict

from metacanon_backend.dbpedia.dbpedia_metadata_retriever import DbpediaListRetriever


class DbpediaWorkListRetriever(DbpediaListRetriever):
    def _run_query(self) -> List[Dict[str, str]]:
        resources = []
        print(f"About to run query: {self._query}")
        response = self._get_response()
        for binding in response["results"]["bindings"]:
            resources.append({
                "author": self._get_value(binding, "author"),
                "title": self._get_value(binding, "title"),
                "resource": self._get_value(binding, "resource"),
                "release_date": self._get_value(binding, "releaseDate"),
                "release_date_2": self._get_value(binding, "releaseDate2"),
                "published": self._get_value(binding, "published"),
                "publication_date": self._get_value(binding, "publicationDate"),
                "publication_date_2": self._get_value(binding, "publicationDate2")
            })
        return resources

    @staticmethod
    def _get_value(binding, field):
        binding = binding.get(field)
        if binding is not None:
            return binding.get("value")
        else:
            return None

    @property
    def _query(self) -> str:
        return "PREFIX dbo: <https://dbpedia.org/ontology/> " \
               "PREFIX dbp: <https://dbpedia.org/property/> " \
               "PREFIX : <https://dbpedia.org/resource/> " \
               "SELECT ?author, ?title, ?resource, ?releaseDate, ?releaseDate2, ?published, " \
               "?publicationDate, ?publicationDate2 { " \
               "?resource a dbo:WrittenWork. " \
               "?resource dbo:author ?author. " \
               "OPTIONAL{?resource dbp:releaseDate ?releaseDate.} " \
               "OPTIONAL{?resource dbo:releaseDate ?releaseDate2.} " \
               "OPTIONAL{?resource dbp:published ?published.} " \
               "OPTIONAL{?resource dbp:publicationDate ?publicationDate.} " \
               "OPTIONAL{?resource dbp:pubDate ?publicationDate2.} " \
               "?resource foaf:name ?title } " \
               f"OFFSET {self._offset} LIMIT {self._limit}"

from SPARQLWrapper import JSON, SPARQLWrapper

from abc import ABCMeta, abstractmethod

from asyncio import sleep
from typing import Dict


class DbpediaMetadataRetriever(metaclass=ABCMeta):
    @property
    @abstractmethod
    def _query(self) -> str:
        pass

    @property
    def _end_point(self) -> str:
        return "https://dbpedia.org/sparql"

    def _get_response(self) -> Dict:
        sparql = SPARQLWrapper(self._end_point)
        sparql.setReturnFormat(JSON)
        sparql.setQuery(self._query)
        sparql.query().convert()
        return sparql.query().convert()

    @staticmethod
    def _resource_name(name: str):
        escaped_chars = ".',?!()*/"
        resource_name = name.replace(" ", "_")
        for c in escaped_chars:
            resource_name = resource_name.replace(c, f"\\{c}")
        return resource_name


class DbpediaListRetriever(DbpediaMetadataRetriever, metaclass=ABCMeta):
    def __init__(self):
        super().__init__()
        self._offset = 0
        self._limit = 1000

    def run(self):
        resources = []
        while True:
            resources_from_query = self._run_query()
            resources.extend(resources_from_query)
            print(f"Partial query found {len(resources_from_query)} resources.")
            if len(resources_from_query) < self._limit:
                break
            else:
                self._offset += self._limit
                sleep(5)

        return resources

    @abstractmethod
    def _run_query(self):
        pass

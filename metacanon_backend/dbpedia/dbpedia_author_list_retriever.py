from typing import List, Dict

from metacanon_backend.dbpedia.dbpedia_metadata_retriever import DbpediaListRetriever


class DbpediaAuthorListRetriever(DbpediaListRetriever):
    def _run_query(self) -> List[Dict[str, str]]:
        resources = []
        print(f"About to run query: {self._query}")
        response = self._get_response()
        for binding in response["results"]["bindings"]:
            resources.append({
                "resource": self._get_value(binding, "author"),
                "gender": self._get_value(binding, "gender"),
                "birth_date": self._get_value(binding, "birthDate"),
                "death_date": self._get_value(binding, "value"),
                "label": self._get_value(binding, "label")
            })
        return resources

    @staticmethod
    def _get_value(binding, field):
        binding = binding.get(field)
        if binding is not None:
            return binding.get("value")
        else:
            return None

    @property
    def _query(self) -> str:
        return "PREFIX dbo: <https://dbpedia.org/ontology/> " \
               "PREFIX dbp: <https://dbpedia.org/property/> " \
               "PREFIX : <https://dbpedia.org/resource/> " \
               "SELECT DISTINCT ?author, ?gender, ?birthDate, ?deathDate, ?label " \
               "{ ?writtenWork a dbo:WrittenWork. " \
               "?writtenWork dbo:author ?author. " \
               "OPTIONAL{?author foaf:gender ?gender.} " \
               "OPTIONAL{?author dbo:birthDate ?birthDate} " \
               "OPTIONAL{?author dbo:deathDate ?deathDate.} " \
               "?author rdfs:label ?label. } " \
               f"ORDER BY ?author OFFSET {self._offset} LIMIT {self._limit}"

    @property
    def _end_point(self) -> str:
        return "https://live.dbpedia.org/sparql"


if __name__ == "__main__":
    DbpediaAuthorListRetriever().run()

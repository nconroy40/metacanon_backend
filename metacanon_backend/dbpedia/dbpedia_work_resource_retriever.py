import argparse
import re
from pprint import pprint
from typing import Optional, Dict

from metacanon_backend.dbpedia.dbpedia_metadata_retriever import DbpediaMetadataRetriever


class DbpediaWorkResourceRetriever(DbpediaMetadataRetriever):
    def __init__(self,
                 title,
                 author_resource):
        self._title = title
        self._author_resource = author_resource

    def run(self) -> Optional[Dict]:
        print(self._query.encode("utf-8"))
        output = {"resource": None, "publication_date": None, "publication_year": None, "wiki_abstract": None}
        response = self._get_response()
        results = response.get("results", None)
        if results is not None:
            bindings = results.get("bindings", None)
            if bindings is not None and len(bindings) > 0:
                output["resource"] = bindings[0].get("work").get("value")
                published = bindings[0].get("published")
                if published is not None:
                    output["publication_date"] = published.get("value")
                    match = re.search(r"(\d{4})", output.get("publication_date"))
                    if match is not None:
                        output["publication_year"] = match.group(1)
                abstract = bindings[0].get("wiki_abstract")
                if abstract is not None:
                    output["wiki_abstract"] = abstract.get("value")

        return output

    @property
    def _query(self) -> str:
        return (
            "PREFIX dbo: <https://dbpedia.org/ontology/> "
            "PREFIX dbp: <https://dbpedia.org/property/> "
            "PREFIX : <https://dbpedia.org/resource/> "
            "SELECT * "
            "WHERE { "
            f":{self._title_resource_name} dbo:wikiPageRedirects * ?disambiguation. "
            "?disambiguation dbo:wikiPageDisambiguates * ?work. "
            f":{self._author_resource_name} dbo:wikiPageRedirects * ?author. "
            "?work dbo:author ?author. "
            "OPTIONAL{?work dbp:releaseDate ?published.} "
            "OPTIONAL{?work dbo:releaseDate ?published.} "
            "OPTIONAL{?work dbo:publicationDate ?published.} "
            "OPTIONAL{?work dbp:pubDate ?published.} "
            "OPTIONAL{?work dbo:abstract ?wiki_abstract.} "
            "BIND(IF((lang(?wiki_abstract) = 'en'),1,0) AS ?abstract_in_english) "
            "} "
            "ORDER BY DESC(?abstract_in_english) "
        )

    @property
    def _title_resource_name(self):
        return self._resource_name(self._title)

    @property
    def _author_resource_name(self):
        return self._resource_name(self._author_resource)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get Dbpedia Metadata')
    parser.add_argument('--author-resource', nargs='?', required=False)
    parser.add_argument('--author_id', nargs='?', required=False)
    parser.add_argument("--title", nargs='?', required=False)
    args = parser.parse_args()
    pprint(
        DbpediaWorkResourceRetriever(author_resource=args.author_resource, title=args.title).run()
    )

from pprint import pprint

# noinspection PyUnresolvedReferences
import wsgi

import argparse
from typing import Optional, Dict

from metacanon_backend.dbpedia.dbpedia_metadata_retriever import DbpediaMetadataRetriever
from metacanon_core.models import Author


class DbpediaAuthorResourceRetriever(DbpediaMetadataRetriever):
    def __init__(self,
                 author_first_name: str,
                 author_last_name: str):
        self._author_first_name = author_first_name
        self._author_last_name = author_last_name

    @property
    def _query(self) -> str:
        return (
            "PREFIX dbo: <https://dbpedia.org/ontology/> "
            "PREFIX dbp: <https://dbpedia.org/property/> "
            "PREFIX : <https://dbpedia.org/resource/> "
            "SELECT * "
            "WHERE { "
            f":{self._author_resource_name} dbo:wikiPageRedirects * ?resource. "
            "?resource a ?type. "
            "OPTIONAL{?resource foaf:gender ?gender.} "
            "FILTER(regex(str(?type), 'Scholar') OR regex(str(?type), 'Writer'))"
            "} "
        )

    @property
    def _author_resource_name(self):
        return self._resource_name(f"{self._author_first_name} {self._author_last_name}")

    def run(self) -> Optional[Dict]:
        print(self._query.encode("utf-8"))

        output = {"resource": '', "gender": ''}
        response = self._get_response()
        results = response.get("results", None)
        if results is not None:
            bindings = results.get("bindings", None)
            if bindings is not None and len(bindings) > 0:
                output["resource"] = bindings[0].get("resource").get("value")
                if bindings[0].get("gender") is not None:
                    output["gender"] = bindings[0].get("gender").get("value")

        if output["resource"] == '':
            return None
        else:
            return output


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Get Library of Congress Metadata')
    parser.add_argument('--first-name', nargs='?', required=False)
    parser.add_argument('--last-name', nargs='?', required=False)
    parser.add_argument("--author_id", nargs='?', required=False)
    args = parser.parse_args()
    if args.author_id is not None:
        author = Author.objects.get(author_id=40)
        first_name = author.first_name
        last_name = author.last_name
    else:
        first_name = args.first_name
        last_name = args.last_name
    pprint(DbpediaAuthorResourceRetriever(author_first_name=first_name, author_last_name=last_name).run())

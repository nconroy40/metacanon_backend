from metacanon_backend.citations.google_scholar_citations_count_retriever import QueryManipulator
from unittest import TestCase

from metacanon_backend.scholar.article import ScholarArticle


class GoogleScholarCitationsCountRetrieverTest(TestCase):

    def setUp(self):
        self.articles = [self._create_scholar_article('Made up book, volume 1', 100, 1234),
                         self._create_scholar_article('Another made up book.', 124, 1233),
                         self._create_scholar_article('Made up book: vol I', 101, 1232),
                         self._create_scholar_article('This is a different book', 23, 1231)]

        self.expected_report = {'best_match_title': 'Made up book: vol I',
                                'best_match_num_citations': 101,
                                'best_match_cluster_id': 1232,
                                'articles_list': [
                                    {
                                        'title': 'Made up book, volume 1',
                                        'num_citations': 100,
                                        'cluster_id': 1234
                                    },
                                    {
                                        'title': 'Another made up book.',
                                        'num_citations': 124,
                                        'cluster_id': 1233
                                    },
                                    {
                                        'title': 'Made up book: vol I',
                                        'num_citations': 101,
                                        'cluster_id': 1232
                                    },
                                    {
                                        'title': 'This is a different book',
                                        'num_citations': 23,
                                        'cluster_id': 1231
                                    }
                                ]}

    @staticmethod
    def _create_scholar_article(title, citations: int, cluster_id: int):
        article = ScholarArticle()
        article.attrs['title'][0] = title
        article.attrs['num_citations'][0] = citations
        article.attrs['cluster_id'][0] = cluster_id
        return article

    def test_find_best_matching_article(self):
        query_manipulator = QueryManipulator('Made up book, volume 1', self.articles)
        best_match = query_manipulator.find_best_matching_article()
        self.assertEqual(best_match.attrs['title'][0], 'Made up book: vol I')

        query_manipulator = QueryManipulator('This is a different book.', self.articles)
        best_match = query_manipulator.find_best_matching_article()
        self.assertEqual(best_match.attrs['title'][0], 'This is a different book')

    def test_generate_report(self):
        query_manipulator = QueryManipulator('Made up book, volume 1', self.articles)
        report = query_manipulator.generate_report()
        self.assertEqual(report, self.expected_report)

from unittest import mock

import pytest
from mock import MagicMock

from metacanon_backend.citations.citation_retrieval_error import UnusualTrafficError
from metacanon_backend.citations.jstor_citations_count_retriever import JstorCitationsCountRetriever


# noinspection PyPep8
def _captcha_response():
    return """
<!DOCTYPE html>
<html class="popup no-js" lang="en">
  <head>
    <meta name="robots" content="noarchive,NOODP" />
    <meta name="description" content="JSTOR is a digital library of academic journals, books, and primary sources." />
    <meta name="viewport" content="width=device-width" />
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="/assets/global_20171026T1134/build/global/css/popup.css" />
    <link rel="apple-touch-icon" href="/assets/global_20171026T1134/build/images/apple-touch-icon.png" />
    <title>JSTOR: Access Check</title>
    <!-- Custom CSS -->
  </head>
  <body>
    <div class="logo-container">
      <a href="/"><img src="/assets/global_20171026T1134/build/images/jstor-logo.png" srcset="/assets/global_20171026T1134/build/images/jstor-logo.png" class="non-responsive" alt="JSTOR Home" width="65" height="90" /></a>
    </div>
    <div id="content" role="main" class="row content brdra">
      <div class="small-12 columns paxl mtxl">
        <div class="row popup-inner">
          <div class="small-12 columns noGlobalSrch">
            <h2>Access Check</h2>
            <p>Our systems have detected unusual traffic activity from your network. Please complete this reCAPTCHA to demonstrate that it's
               you making the requests and not a robot. If you are having trouble seeing or completing this challenge,
               <a href="https://support.jstor.org/hc/en-us/articles/115011068868-Troubleshooting-CAPTCHA-" target="_blank" title="This link opens in a new window">this page</a> may help.
               If you continue to experience issues, you can <a href="https://support.jstor.org/" target="_blank" title="This link opens in a new window">contact JSTOR support</a>.</p>
            <div id="px-captcha"> </div>
            <p>Block Reference: #c413f6b0-3b95-11ea-a017-f56b62d2424f<br/>
               VID: #(null)<br/>
               IP: 207.154.223.24<br/>
               Date and time: Mon, 20 Jan 2020 15:01:41 GMT<br/>
               <noscript>Javascript is disabled</noscript></p>
            <p>Go back to <a href="/" title="Go back to JSTOR">JSTOR</a></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="small-12 columns pts">
        <small>&copy;2000-<script type="text/javascript">document.write(new Date().getFullYear());</script> ITHAKA. All Rights Reserved. JSTOR&reg;, the JSTOR logo, JPASS&reg;, and ITHAKA&reg; are registered trademarks of ITHAKA.</small>
      </div>
    </div>
    <!-- Px --> <script> window._pxAppId = 'PXu4K0s8nX'; window._pxJsClientSrc = '/u4K0s8nX/init.js'; window._pxFirstPartyEnabled = true; window._pxVid = ''; window._pxUuid = 'c413f6b0-3b95-11ea-a017-f56b62d2424f'; window._pxHostUrl = '/u4K0s8nX/xhr'; </script>
    <script> var s = document.createElement('script'); s.src = '/u4K0s8nX/captcha/captcha.js?a=c&u=c413f6b0-3b95-11ea-a017-f56b62d2424f&v=&m=0'; var p = document.getElementsByTagName('head')[0]; p.insertBefore(s, null); if (true ){s.onerror = function () {s = document.createElement('script'); var suffixIndex = '/u4K0s8nX/captcha/captcha.js?a=c&u=c413f6b0-3b95-11ea-a017-f56b62d2424f&v=&m=0'.indexOf('/captcha.js'); var temperedBlockScript = '/u4K0s8nX/captcha/captcha.js?a=c&u=c413f6b0-3b95-11ea-a017-f56b62d2424f&v=&m=0'.substring(suffixIndex); s.src = '//captcha.px-cdn.net/PXu4K0s8nX' + temperedBlockScript; p.parentNode.insertBefore(s, p);};}</script>
    <!-- Custom Script -->
  </body>
</html>
"""


# noinspection PyPep8
def _no_results_response():
    return """
<!DOCTYPE html>
<html class=" no-js" lang="en">
    <head></head>
    <body>
    <div id="search-results-module">
        <main aria-label="Search results" id="search-results-container">
            <a href="#facet-filters" class="visuallyhidden">This is the search results section. Skip to filters section.</a>
            
    
            <noscript>
                <div class="notify-error mbl">You do not have JavaScript enabled. Please enable JavaScript to use all features on this page.</div>
            </noscript>
    
            <div id="search-container" data-ajax-search="true" data-modified-query="((&quot;Toni Morrison&quot;) OR (&quot;Morrison, Toni&quot;)) AND (&quot;Begroved&quot;)">
                
                <div class="small-12 large-9 columns phn">
                    
                        <h2>No results found</h2>
                        <span class="medium-heading">Try these suggestions:</span>
                        <ul class="mlxl mbxl">
                            <li>Check your spelling</li>
                            
                            <li>Clear your search filters</li>
                            <li>See <a href="//support.jstor.org/core-functionality" class="sc-searchHelp" data-qa="search-help-link" target="_blank" title="This link opens in a new window">Search Help</a></li>
                        </ul>
                    
                </div>
            </div>
            
                <div id="loading-image" class="loading-image hide" aria-live="assertive">
                    <img src="//www.jstor.org/assets/global_20200117T1649/build/images/search-results-loading.gif" alt="Fetching your results...">
                </div>
                <div id="loading-cover" class="loading-results hide" aria-hidden="true"></div>
            
    
            
        </main>
    </div>
    </body>
</html>
"""


def _malformed_response():
    return """
<!DOCTYPE html>
<html class=" no-js" lang="en">
    <head></head>
    <body>
    <div id="search-results-module">
        <p>some random text</p>
    </div>
    </body>
</html>
"""


@mock.patch("metacanon_backend.citations.citations_count_retriever_base.requests.get")
def test_failed_on_captcha(mock_get):
    response = MagicMock()
    response.text = _captcha_response()
    mock_get.return_value = response
    retriever = JstorCitationsCountRetriever("Toni", "Morrison", ["Beloved"])
    with pytest.raises(UnusualTrafficError):
        retriever.get_num_citations()


@mock.patch("metacanon_backend.citations.citations_count_retriever_base.requests.get")
def test_no_citations_found(mock_get):
    response = MagicMock()
    response.text = _no_results_response()
    mock_get.return_value = response
    retriever = JstorCitationsCountRetriever("Toni", "Morrison", ["Not a real book"])
    assert retriever.get_num_citations() == 0


@mock.patch("metacanon_backend.citations.citations_count_retriever_base.requests.get")
def test_failed_with_unknown_error(mock_get):
    response = MagicMock()
    response.text = _malformed_response()
    mock_get.return_value = response
    retriever = JstorCitationsCountRetriever("Toni", "Morrison", ["Not a real book"])
    with pytest.raises(UnusualTrafficError):
        retriever.get_num_citations()

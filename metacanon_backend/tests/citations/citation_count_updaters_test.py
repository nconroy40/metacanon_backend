import pytest
from mock import Mock

from metacanon_backend.citations.citation_count_updaters import update_citation_count
from metacanon_backend.citations.citation_retrieval_error import CitationRetrievalError
from metacanon_core.models import JSTORCitationCount, Work, Region, Author


def _create_default_work():
    region1 = Region(name='region1', human_readable_name="Region 1", access_level=1)
    region1.save()
    author = Author(first_name="First", last_name="Last")
    author.save()
    work = Work(title='work1',
                author_id=author,
                publication_year=1934,
                genres=[1, 2],
                format="book",
                tags=[1],
                region=region1,
                language="English")
    work.save()
    return work


@pytest.mark.django_db
def test_update_citation_count():
    mock_get_num_citations_method = Mock()
    mock_get_num_citations_method.return_value = 4
    work = _create_default_work()
    retriever = Mock()
    retriever.get_num_citations = mock_get_num_citations_method
    update_citation_count(
        citation_class=JSTORCitationCount,
        citation_class_entry_id=1,
        get_num_citations_method="get_num_citations",
        retriever=retriever,
        force=False
    )
    assert JSTORCitationCount.objects.get(work_id=work).count == 4


@pytest.mark.django_db
def test_doesnt_update_if_difference_too_great():
    mock_get_num_citations_method = Mock()
    mock_get_num_citations_method.return_value = 100
    work = _create_default_work()
    retriever = Mock()
    retriever.get_num_citations = mock_get_num_citations_method
    update_citation_count(
        citation_class=JSTORCitationCount,
        citation_class_entry_id=1,
        get_num_citations_method="get_num_citations",
        retriever=retriever,
        force=False
    )
    assert JSTORCitationCount.objects.get(work_id=work).count == 100

    # New value is too small, so we don't update.
    mock_get_num_citations_method.return_value = 0
    with pytest.raises(CitationRetrievalError):
        update_citation_count(
            citation_class=JSTORCitationCount,
            citation_class_entry_id=1,
            get_num_citations_method="get_num_citations",
            retriever=retriever,
            force=False
        )
    assert JSTORCitationCount.objects.get(work_id=work).count == 100

import datetime
from typing import Union

from metacanon_backend.citations.attempt_query import AttemptsState, AttemptsStateEntry

TEST_WEIGHTS = {
    (10, 1, 9): {"0": 9.900000000000001e-05, "1": -0.09001},
    (6, 1, 9): {"0": 10.609502790723615, "1": 15.11929113192085},
    (6, 0, 9): {"0": 0.1024555512098302, "1": -0.1891099},
    (7, 0, 9): {"0": 0.265142153055239, "1": -0.6702528003294614},
    (8, 0, 9): {"0": 0.31777796427276, "1": -0.06871501056106628},
    (9, 0, 9): {"0": 0.09313804116676912, "1": -0.0026108351503871946},
    (10, 0, 9): {"0": 0.3965167932502762, "1": 0.21134177867488013},
    (11, 0, 9): {"0": 0.003148024204361435, "1": -0.02646330140937582},
    (12, 0, 9): {"0": 0.005266595774696025, "1": -0.4560073897885898},
    (13, 0, 9): {"0": 0.11541522071850702, "1": 0.09002256553652847},
    (12, 0, 8): {"0": 0.5468376301888688, "1": -1.1394547260646468},
    (6, 0, 8): {"0": 0.010701979744464282, "1": -0.18898566018627894},
    (7, 0, 8): {"0": 0.023270601555806087, "1": -0.09010520833318186},
    (8, 0, 8): {"0": 0.039667143343690675, "1": -0.18920333851426158},
    (9, 0, 8): {"0": 0.058116100353671325, "1": 0.031526557908306815},
    (10, 0, 8): {"0": 0.11747384676995114, "1": -0.844587921968634}
}


def test_weights(tmpdir):
    path = tmpdir.mkdir("data")
    attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2020, 1, 1, 1, 0, 0, 0)
    )

    # Should start with empty weights.
    assert attempts_state.weights == {}

    _update(attempts_state, datetime.datetime(2020, 1, 1, 1, 0, 0, 0), attempted=True)

    # Weights updated in memory.
    assert attempts_state.weights == TEST_WEIGHTS

    attempts_state.save()

    new_attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2020, 1, 1, 1, 0, 0, 0)
    )

    # Weights read from disk.
    assert new_attempts_state.weights == TEST_WEIGHTS


def test_num_recent_queries(tmpdir):
    path = tmpdir.mkdir("data")
    attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2020, 1, 1, 1, 0, 0, 0)
    )
    assert attempts_state.num_recent_queries == 0

    _update(attempts_state, datetime.datetime(2020, 1, 1, 1, 0, 0, 0), attempted=1)

    assert attempts_state.num_recent_queries == 1

    _update(attempts_state, datetime.datetime(2020, 1, 1, 2, 0, 0, 0), attempted=False)
    _update(attempts_state, datetime.datetime(2020, 1, 1, 3, 0, 0, 0), attempted=True)

    assert attempts_state.num_recent_queries == 2

    _update(attempts_state, datetime.datetime(2020, 1, 1, 4, 0, 0, 0), attempted=True)

    assert attempts_state.num_recent_queries == 3

    _update(attempts_state, datetime.datetime(2020, 1, 1, 13, 0, 0, 0), attempted=True)

    assert attempts_state.num_recent_queries == 3

    attempts_state.save()

    new_attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2020, 1, 1, 13, 0, 0, 0),
    )

    assert new_attempts_state.num_recent_queries == 3

    _update(new_attempts_state, datetime.datetime(2020, 1, 1, 16, 0, 0, 0), attempted=True)

    assert new_attempts_state.num_recent_queries == 2  # Previous should get filtered out.


def test_last_attempt_last_recorded(tmpdir):
    path = tmpdir.mkdir("data")
    attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2019, 12, 25, 1, 0, 0, 0)
    )

    assert attempts_state.last_recorded is None
    assert attempts_state.last_attempt is None

    _update(attempts_state, datetime.datetime(2019, 12, 25, 1, 0, 0, 0), attempted=True)

    first = AttemptsStateEntry(
        timestamp=datetime.datetime(2019, 12, 25, 1, 0, 0, 0),
        attempted=True,
        blocked=False,
        seconds_since_last=10,
        last_was_successful=True,
        num_recent_queries=10,
    )

    assert attempts_state.last_recorded == first
    assert attempts_state.last_attempt == first
    attempts_state.save()

    attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2019, 12, 26, 1, 0, 0, 0)
    )

    # The existing entry is too old to be considered recent, but it still exists in the file.
    assert attempts_state.num_recent_queries == 0
    assert attempts_state.last_recorded == first
    assert attempts_state.last_attempt == first
    attempts_state.save()

    attempts_state = AttemptsState(
        path=path,
        query_type="test_query_type",
        now=datetime.datetime(2019, 12, 27, 1, 0, 0, 0)
    )

    # The existing entry is now two days old so we don't include it at all.
    assert attempts_state.last_recorded is None
    assert attempts_state.last_attempt is None

    _update(attempts_state, datetime.datetime(2020, 1, 1, 1, 0, 0, 0), attempted=True)
    _update(attempts_state, datetime.datetime(2020, 1, 1, 2, 0, 0, 0), attempted=False)
    _update(attempts_state, datetime.datetime(2020, 1, 1, 3, 0, 0, 0), attempted=True)
    _update(attempts_state, datetime.datetime(2020, 1, 1, 4, 0, 0, 0), attempted=True)
    _update(attempts_state, datetime.datetime(2020, 1, 1, 13, 0, 0, 0), attempted=False)

    assert attempts_state.last_recorded == AttemptsStateEntry(
        timestamp=datetime.datetime(2020, 1, 1, 13, 0, 0, 0),
        attempted=False,
        blocked=False,
        seconds_since_last=10,
        last_was_successful=True,
        num_recent_queries=10,
    )

    assert attempts_state.last_attempt == AttemptsStateEntry(
        timestamp=datetime.datetime(2020, 1, 1, 4, 0, 0, 0),
        attempted=True,
        blocked=False,
        seconds_since_last=10,
        last_was_successful=True,
        num_recent_queries=10,
    )


def _update(attempts_state: AttemptsState, now: datetime, attempted: Union[int, bool]):
    attempts_state.update(
        timestamp=now,
        attempted=int(attempted),
        blocked=False,
        rl_model=TEST_WEIGHTS,
        seconds_since_last=10,
        last_was_successful=True,
        num_recent_queries=10
    )

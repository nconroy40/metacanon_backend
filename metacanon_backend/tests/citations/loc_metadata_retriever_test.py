from metacanon_backend.citations.loc_metadata_retriever import year_from_loc_date


def test_year_from_loc_date():
    assert year_from_loc_date("New York : Grove Press, 1984, c1978.") == "1984"
    assert year_from_loc_date("New York : Grove Press, 1984") == "1984"
    assert year_from_loc_date("New York : Grove Press, 1984, p1940") == "1940"
    assert year_from_loc_date("New York : Grove Press 13456, 1995: 333333") == "1995"


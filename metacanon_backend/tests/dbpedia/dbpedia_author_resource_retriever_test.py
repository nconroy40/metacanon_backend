from mock import patch, MagicMock

from metacanon_backend.dbpedia.dbpedia_author_resource_retriever import DbpediaAuthorResourceRetriever


@patch('metacanon_backend.dbpedia.dbpedia_metadata_retriever.SPARQLWrapper')
def test_retriever(mock_sparql_wrapper_class):
    mock_sparql_wrapper_class.return_value = MagicMock(
        query=MagicMock(return_value=MagicMock(convert=MagicMock(
            return_value={
                'head': {'link': [], 'vars': ['resource', 'type']},
                'results': {
                    'distinct': False,
                    'ordered': True,
                    'bindings': [
                        {
                            'resource': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Jimmy_Johns'},
                            'type': {'type': 'uri', 'value': 'https://dbpedia.org/class/yago/Scholar110557854'},
                            'gender': {'type': 'literal', 'value': 'male', 'xml:lang': 'en'}
                        },
                        {
                            'resource': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Jimmy_Jonhs'},
                            'type': {'type': 'uri', 'value': 'https://dbpedia.org/ontology/Writer'},
                            'gender': {'type': 'literal', 'value': 'male', 'xml:lang': 'en'}
                        },
                    ]
                }
            }
        ))))

    retriever = DbpediaAuthorResourceRetriever(author_first_name="Jimmy", author_last_name="Johns")
    assert retriever.run() == {'resource': 'https://dbpedia.org/resource/Jimmy_Johns', 'gender': 'male'}


@patch('metacanon_backend.dbpedia.dbpedia_metadata_retriever.SPARQLWrapper')
def test_retriever_null(mock_sparql_wrapper_class):
    mock_sparql_wrapper_class.return_value = MagicMock(
        query=MagicMock(return_value=MagicMock(convert=MagicMock(return_value={
            'head': {'link': [], 'vars': ['resource', 'type']},
            'results': {'distinct': False, 'ordered': True, 'bindings': []}
        }
        ))))

    retriever = DbpediaAuthorResourceRetriever(author_first_name="Jimmy", author_last_name="Johns")
    assert retriever.run() is None

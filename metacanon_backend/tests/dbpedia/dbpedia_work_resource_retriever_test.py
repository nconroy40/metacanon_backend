from mock import patch, MagicMock

from metacanon_backend.dbpedia.dbpedia_work_resource_retriever import DbpediaWorkResourceRetriever


@patch('metacanon_backend.dbpedia.dbpedia_metadata_retriever.SPARQLWrapper')
def test_retriever(mock_sparql_wrapper_class):
    mock_sparql_wrapper_class.return_value = MagicMock(
        query=MagicMock(return_value=MagicMock(convert=MagicMock(return_value={
            'head':
                {
                    'link': [],
                    'vars':
                        ['work', 'author', 'published', 'wiki_abstract', 'disambiguation', 'abstract_in_english']
                },
            'results': {
                'distinct': False,
                'ordered': True,
                'bindings': [
                    {'work': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Beloved_(novel)'},
                     'author': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Toni_Morrison'},
                     'published': {'type': 'typed-literal',
                                   'datatype': 'https://www.w3.org/1999/02/22-rdf-syntax-ns#langString',
                                   'value': 'September 1987'},
                     'wiki_abstract': {'type': 'literal', 'xml:lang': 'en', 'value': 'A long abstract.'},
                     'disambiguation': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Beloved'},
                     'abstract_in_english': {'type': 'typed-literal',
                                             'datatype': 'https://www.w3.org/2001/XMLSchema#integer', 'value': '1'}
                     },
                    {'work': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Beloved_(novel)'},
                     'author': {'type': 'uri', 'value': 'https://dbpedia.org/resource/Toni_Morrison'},
                     'published': {'type': 'typed-literal',
                                   'datatype': 'https://www.w3.org/1999/02/22-rdf-syntax-ns#langString',
                                   'value': 'September 1987'},
                     'wiki_abstract': {'type': 'literal', 'xml:lang': 'fr',
                                       'value': "Beloved (titre original : Beloved) est un roman..."},
                     'abstract_in_english': {'type': 'typed-literal',
                                             'datatype': 'https://www.w3.org/2001/XMLSchema#integer', 'value': '0'}
                     }
                ]}
        }
        ))))

    retriever = DbpediaWorkResourceRetriever(author_resource="Toni_Morrison", title="Beloved")
    assert retriever.run() == {
        'publication_date': 'September 1987',
        'publication_year': '1987',
        'resource': 'https://dbpedia.org/resource/Beloved_(novel)',
        'wiki_abstract': 'A long abstract.'
    }


@patch('metacanon_backend.dbpedia.dbpedia_metadata_retriever.SPARQLWrapper')
def test_retriever_null(mock_sparql_wrapper_class):
    mock_sparql_wrapper_class.return_value = MagicMock(
        query=MagicMock(return_value=MagicMock(convert=MagicMock(return_value={
            'head': {'link': [], 'vars': ['resource', 'type']},
            'results': {'distinct': False, 'ordered': True, 'bindings': []}
        }
        ))))

    retriever = DbpediaWorkResourceRetriever(author_resource="Toni_Morrison", title="Beloved")
    assert retriever.run() == {
        'publication_date': None,
        'publication_year': None,
        'resource': None,
        'wiki_abstract': None
    }

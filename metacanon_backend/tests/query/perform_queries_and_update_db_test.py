import pytest
from mock import patch, Mock, call

from metacanon_backend.ai import rl_configs
from metacanon_backend.citations.query_types import SourceAttributes, QueryType
from metacanon_core.models import QueryQueue, Work
from metacanon_backend.query.perform_queries_and_update_db import query_and_update
from metacanon_core.tests.test_utils import create_db_defaults


# noinspection PyUnusedLocal
def _fake_attempt_query_method(query_function, attempts_state, config):
    return query_function()


# noinspection PyUnusedLocal
@pytest.mark.django_db
@patch('metacanon_backend.query.perform_queries_and_update_db.attempt_query', new=_fake_attempt_query_method)
@patch('metacanon_backend.query.perform_queries_and_update_db.RL_AGENT_DATA_ROOT')
@patch('metacanon_backend.query.perform_queries_and_update_db.sleep')
@patch('metacanon_backend.query.perform_queries_and_update_db.update_citation_count')
@patch('metacanon_backend.query.perform_queries_and_update_db.query_types')
def test_perform_queries_and_update_db(query_types, update_citation_count, sleep, rl_agent_data_root):
    query_types.get_source_attributes.return_value = SourceAttributes(
        query_types=("test_query_type", "second_query_type"),
        limit=2,
        sleep_delay=1.0
    )

    fake_retriever_class = Mock()
    fake_retriever_class.get_num_citations = lambda: 5
    fake_retriever_class.get_num_citations_2 = lambda: 6
    fake_retriever = Mock()
    fake_retriever_class.return_value = fake_retriever
    citation_count_type = Mock()
    citation_count_type_2 = Mock()

    query_type = QueryType(
        retriever_class=fake_retriever_class,
        query_method="get_num_citations",
        related_type=Work,
        citation_count_type=citation_count_type,
        update_attempt_model_config=rl_configs.hathi()
    )

    second_query_type = QueryType(
        retriever_class=fake_retriever_class,
        query_method="get_num_citations_2",
        related_type=Work,
        citation_count_type=citation_count_type_2,
        update_attempt_model_config=rl_configs.hathi()
    )

    def get_query_type(query_type_string: str):
        if query_type_string == "test_query_type":
            return query_type
        elif query_type_string == "second_query_type":
            return second_query_type
        else:
            assert False

    query_types.get = Mock(side_effect=get_query_type)

    create_db_defaults()
    query_still_in_queue = QueryQueue(
        work_id=2,
        author_id=None,
        query_args={"query_arg_1": "another_first_arg", "query_arg_2": "another_second_arg"},
        query_type="third_query_type",
        force_query=False,
    )
    query_still_in_queue.save()
    QueryQueue(
        work_id=1,
        author_id=None,
        query_args={"query_arg_1": "first_arg", "query_arg_2": "second_arg"},
        query_type="test_query_type",
        force_query=False,
    ).save()
    QueryQueue(
        work_id=2,
        author_id=None,
        query_args={"query_arg_1": "another_first_arg", "query_arg_2": "another_second_arg"},
        query_type="second_query_type",
        force_query=False,
    ).save()
    additional_query = QueryQueue(
        work_id=2,
        author_id=None,
        query_args={"query_arg_1": "another_first_arg", "query_arg_2": "another_second_arg"},
        query_type="second_query_type",
        force_query=False,
    )
    additional_query.save()

    query_and_update("test_source")

    assert list(QueryQueue.objects.all()) == [query_still_in_queue, additional_query]
    fake_retriever_class.assert_has_calls(
        [
            call(**{"query_arg_1": "first_arg", "query_arg_2": "second_arg", "verbose": True}),
            call(query_arg_1="another_first_arg", query_arg_2="another_second_arg", verbose=True)
        ]
    )

    update_citation_count.assert_has_calls(
        [
            call(
                citation_class=citation_count_type,
                citation_class_entry_id=1,
                get_num_citations_method="get_num_citations",
                retriever=fake_retriever,
                force=False,
            ),
            call(
                citation_class=citation_count_type_2,
                citation_class_entry_id=2,
                get_num_citations_method="get_num_citations_2",
                retriever=fake_retriever,
                force=False,
            )
        ]
    )

    sleep.assert_has_calls(
        [call(1.0), call(1.0)]
    )

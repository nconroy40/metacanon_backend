from typing import List

import pytest
from datetime import datetime
from django.utils import timezone

import numpy as np

from metacanon_core.models import Work
from metacanon_core.models.citation_count import HathiTrustTitleFrequency
from metacanon_core.models.query_queue import QueryQueue
from metacanon_backend.query.build_query_queue import build_query_queue
from metacanon_core.tests.test_utils import create_db_defaults


@pytest.mark.django_db
def test_nyt():
    expected = [
        ("nyt_citation_count", 1, None, False),
        ("nyt_citation_count", 2, None, False),
        ("nyt_citation_count", 3, None, False)
    ]
    create_db_defaults()
    build_query_queue("nyt_citation_count", force=False)
    verify_query_queue(
        expected=expected
    )


@pytest.mark.django_db
def test_multiple_types():
    create_db_defaults()
    expected = [
        ("nyt_citation_count", 1, None, False),
        ("nyt_citation_count", 2, None, False),
        ("nyt_citation_count", 3, None, False),
        ("hathi_author_frequency", None, 1, False),
        ("hathi_author_frequency", None, 2, False),
    ]
    build_query_queue("nyt_citation_count", force=False)
    build_query_queue("hathi_author_frequency", force=False)
    # This shouldn't add anything since entries for this type already exist
    build_query_queue("hathi_author_frequency", force=False)
    verify_query_queue(
        expected=expected
    )


def converter(item: QueryQueue):
    return item.query_type, item.work_id, item.author_id, item.force_query


def verify_query_queue(expected: List) -> None:
    assert list(map(converter, QueryQueue.objects.all())) == expected


@pytest.mark.django_db
def test_some_citations_already_exist():
    create_db_defaults()

    _add_hathi_title_frequency_to_db()

    build_query_queue("hathi_title_frequency", force=False)

    verify_query_queue([('hathi_title_frequency', 3, None, False)])


@pytest.mark.django_db
def test_force_query():
    create_db_defaults()

    _add_hathi_title_frequency_to_db()

    build_query_queue("hathi_title_frequency", force=True)

    # All items added regardless of prior status and with force_query set to true.
    verify_query_queue([
        ('hathi_title_frequency', 1, None, True),
        ('hathi_title_frequency', 2, None, True),
        ('hathi_title_frequency', 3, None, True)
    ])


def _add_hathi_title_frequency_to_db():
    np.random.seed(236)
    HathiTrustTitleFrequency(
        work_id=Work.objects.get(work_id=1),
        count=5,
        last_updated=datetime(2019, 1, 1, 5, tzinfo=timezone.get_current_timezone()),
        last_queried=datetime(2020, 3, 15, 5, tzinfo=timezone.get_current_timezone())
    ).save()
    HathiTrustTitleFrequency(
        work_id=Work.objects.get(work_id=2),
        count=5,
        last_updated=datetime(2019, 1, 1, 5, tzinfo=timezone.get_current_timezone()),
        last_queried=datetime(2019, 1, 3, 5, tzinfo=timezone.get_current_timezone())
    ).save()
    HathiTrustTitleFrequency(
        work_id=Work.objects.get(work_id=3),
        count=5,
        last_updated=datetime(2019, 1, 1, 5, tzinfo=timezone.get_current_timezone()),
        last_queried=datetime(2019, 1, 3, 5, tzinfo=timezone.get_current_timezone())
    ).save()

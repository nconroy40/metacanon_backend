import numpy as np
import random
from math import log

from typing import Dict, List


class ModelConfig:
    def __init__(
            self,
            alpha: float,
            gamma: float,
            epsilon_after_success: float,
            epsilon_after_failure: float,
            success_reward: int,
            no_op_reward: int,
            blocked_reward: int):
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon_after_success = epsilon_after_success
        self.epsilon_after_failure = epsilon_after_failure
        self.success_reward = success_reward
        self.no_op_reward = no_op_reward
        self.blocked_reward = blocked_reward


class Model:
    def __init__(
            self,
            config: ModelConfig,
            weights: Dict = None):
        self.config = config
        self.q = {} if weights is None else weights

    def get_decision(self,
                     seconds_since_last: float,
                     last_was_successful: bool,
                     num_recent_queries: int):
        state = self.encode_state(seconds_since_last, last_was_successful, num_recent_queries)
        try:
            preferred_action = self._max_dict(self.q[state])
        except KeyError:
            self.q[state] = {"0": 0, "1": 0.01}
            preferred_action = self._max_dict(self.q[state])

        # Agent is more willing to explore if the last query was a success.
        if state[1] == 0:
            epsilon = self.config.epsilon_after_failure
        else:
            epsilon = self.config.epsilon_after_success

        if np.random.random() < epsilon:
            return random.choice([0, 1])
        else:
            return int(preferred_action)

    @staticmethod
    def state_info(seconds_since_last, last_was_successful, num_recent_queries) -> List[str]:
        state = Model.encode_state(seconds_since_last, last_was_successful, num_recent_queries)
        return [
            f"{seconds_since_last} seconds since last query.",
            f"{num_recent_queries} recent queries.",
            str(state)
        ] + Model.state_to_string(state)

    def update(self,
               last_state,
               state,
               last_action,
               action) -> None:
        try:
            last_value = self.q[last_state][str(last_action)]
        except KeyError:
            self.q[last_state] = {"0": 0, "1": 0.01}
            last_value = self.q[last_state][str(last_action)]

        try:
            this_value = self.q[state][str(action)]
        except KeyError:
            self.q[state] = {"0": 0, "1": 0.01}
            this_value = self.q[state][str(action)]

        if last_action:
            if state[1]:
                reward = self.config.success_reward
            else:
                reward = self.config.blocked_reward
        else:
            reward = self.config.no_op_reward

        self.q[last_state][str(last_action)] += self.config.alpha * \
            (reward + self.config.gamma * this_value - last_value)

    @staticmethod
    def state_to_string(state) -> List[str]:
        less_than_seconds = int(pow(2, state[0]))
        output = [f"Less than {less_than_seconds} seconds since last query."]

        if state[1]:
            output += ["Last query successful."]
        else:
            output += ["Last query failed."]

        less_than_recent_queries = int(pow(2, state[2]))
        output += [f"Less than {less_than_recent_queries} queries in the last 12 hours."]

        return output

    @staticmethod
    def _max_dict(d):
        max_key = None
        max_val = float('-inf')
        for k, v in d.items():
            if v > max_val:
                max_val = v
                max_key = k
        return max_key

    @staticmethod
    def encode_state(
            seconds_since_last: float,
            last_was_successful: bool,
            num_recent_queries: int):
        try:
            time = int(log(Model._round_up(int(seconds_since_last)), 2))
        except ValueError:
            time = 0
        last_was_successful = int(last_was_successful)
        try:
            query_freq = int(log(Model._round_up(int(num_recent_queries)), 2))
        except ValueError:
            query_freq = 0
        return time, last_was_successful, query_freq

    @staticmethod
    def _round_up(seconds: int):
        seconds -= 1
        seconds |= (seconds >> 1)
        seconds |= (seconds >> 2)
        seconds |= (seconds >> 4)
        seconds |= (seconds >> 8)
        seconds |= (seconds >> 16)
        seconds |= (seconds >> 32)
        seconds += 1
        return seconds

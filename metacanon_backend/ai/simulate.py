from datetime import datetime, timedelta

from metacanon_backend.ai.reinforcement_learning_agent import Model, ModelConfig


def main():
    config = ModelConfig(
        alpha=0.01,
        gamma=0.99,
        epsilon_after_success=0.1,
        epsilon_after_failure=0.01,
        success_reward=1,
        no_op_reward=0,
        blocked_reward=-10
    )

    model = Model(config=config)
    time = datetime.now()
    seconds_since_last = 60
    last_was_successful = True
    num_recent_queries = 0
    num_queries_in_last_hour = 0
    state = model.encode_state(seconds_since_last=0, last_was_successful=True, num_recent_queries=0)
    action = 1
    queries = set()

    for i in range(10000):
        last_state = state
        last_action = action

        state = model.encode_state(
            seconds_since_last=seconds_since_last,
            last_was_successful=last_was_successful,
            num_recent_queries=num_recent_queries)

        action = model.get_decision(
            seconds_since_last=seconds_since_last,
            last_was_successful=last_was_successful,
            num_recent_queries=num_recent_queries
        )

        model.update(action=int(action), last_action=int(last_action), state=state, last_state=last_state)

        if action:
            if num_recent_queries < 30 and num_queries_in_last_hour < 5 and seconds_since_last > 200:
                last_was_successful = True
            else:
                last_was_successful = False
            seconds_since_last = 60
            queries.add(time)
        else:
            seconds_since_last += 60

        queries = set([q for q in queries if q > time - timedelta(hours=12)])
        num_recent_queries = len(queries)
        num_queries_in_last_hour = len([q for q in queries if q > time - timedelta(hours=1)])
        time += timedelta(seconds=60)


if __name__ == "__main__":
    main()

from metacanon_backend.ai.reinforcement_learning_agent import ModelConfig


def google_scholar():
    return ModelConfig(
        alpha=0.01,
        gamma=0.99,
        epsilon_after_success=0.1,
        epsilon_after_failure=0.0015,
        success_reward=1,
        no_op_reward=0,
        blocked_reward=-10
    )


def jstor():
    return ModelConfig(
        alpha=0.01,
        gamma=0.99,
        epsilon_after_success=0.1,
        epsilon_after_failure=0.01,
        success_reward=1,
        no_op_reward=0,
        blocked_reward=-10
    )


def hathi():
    return ModelConfig(
        alpha=0.01,
        gamma=0.99,
        epsilon_after_success=0.1,
        epsilon_after_failure=0.01,
        success_reward=1,
        no_op_reward=0,
        blocked_reward=-10
    )


def loc():
    return ModelConfig(
        alpha=0.01,
        gamma=0.99,
        epsilon_after_success=0.1,
        epsilon_after_failure=0.01,
        success_reward=1,
        no_op_reward=0,
        blocked_reward=-10
    )

# noinspection PyUnresolvedReferences
import wsgi

from metacanon_backend.citations.google_scholar_citations_count_retriever import GoogleScholarCitationsCountRetriever

import argparse


def main():
    parser = argparse.ArgumentParser(description='Get citations counts.')
    parser.add_argument('--first_name', nargs='?', required=True)
    parser.add_argument('--last_name', nargs='?', required=True)
    parser.add_argument('--title', nargs='?', required=True)
    parser.add_argument('--alt_titles', nargs='*')

    args = parser.parse_args()

    first = args.first_name
    last = args.last_name
    title = args.title

    retriever = GoogleScholarCitationsCountRetriever(
        author_first_name=first, author_last_name=last, title=title
    )
    print(f"Url: {retriever.url()}.")
    google_scholar_citations = retriever.get_google_scholar_report()
    if google_scholar_citations['best_match_num_citations'] is not None:
        print("Google Scholar: %s." % google_scholar_citations['best_match_num_citations'])
        print("Google Scholar best match title: %s" % google_scholar_citations['best_match_title'])
    else:
        print("Couldn't find Google Scholar article with matching title.")
    print("")
    print("All google scholar articles:")
    print("")
    for article in google_scholar_citations['articles_list']:
        try:
            print("Title: %s" % article['title'])
        except UnicodeEncodeError as e:
            print(e)
        print("Num citations: %s" % article['num_citations'])


if __name__ == "__main__":
    main()

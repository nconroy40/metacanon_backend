import logging

from time import sleep

# noinspection PyUnresolvedReferences
import wsgi
from metacanon_backend.citations.hathi_trust_citations_count_retriever import HathiTrustCitationsCountRetriever

from metacanon_backend.citations.jstor_citations_count_retriever import JstorCitationsCountRetriever

import argparse

from metacanon_backend.citations.nyt_citations_count_retriever import NytCitationsCountRetriever
from metacanon_core.models import Work, Author


def main():
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    logging.getLogger("django.db.backends").setLevel(logging.ERROR)
    logger = logging.getLogger("get_citation_counts")
    logger.setLevel(logging.INFO)

    parser = argparse.ArgumentParser(description='Get citations counts.')
    parser.add_argument('--first-name', nargs='?', required=False)
    parser.add_argument('--last-name', nargs='?', required=False)
    parser.add_argument('--title', nargs='?', required=False)
    parser.add_argument('--alt-titles', nargs='*')
    parser.add_argument('--work-id', nargs='?', required=False)
    parser.add_argument('--publication-year', nargs='?', required=True)
    parser.add_argument('--source', nargs='?', required=True)

    args = parser.parse_args()

    if args.work_id is None:
        first = args.first_name
        last = args.last_name
        titles = [args.title] + args.alt_titles if args.alt_titles else [args.title]
    else:
        work = Work.objects.get(work_id=args.work_id)
        author = Author.objects.get(author_id=work.author_id)
        first = author.first_name
        last = author.last_name
        titles = [work.title] + work.alternate_titles if len(work.alternate_titles) > 0 else [args.title]

    if args.source == "jstor":
        # session = requests.Session()
        session = None

        jstor_retriever = JstorCitationsCountRetriever(author_first_name=first,
                                                       author_last_name=last,
                                                       titles=titles,
                                                       verbose=True,
                                                       session=session)
        jstor_citations = jstor_retriever.get_num_citations()
        print("JSTOR: %s." % jstor_citations)
        sleep(1)

        jstor_lit_citations = jstor_retriever.get_num_lit_citations()
        print("JSTOR Language and Literature: %s" % jstor_lit_citations)
        sleep(1)

        jstor_alh_citations = jstor_retriever.get_num_alh_citations()
        print("JSTOR ALH: %s" % jstor_alh_citations)
        sleep(1)

        jstor_al_citations = jstor_retriever.get_num_al_citations()
        print("JSTOR American Literature: %s" % jstor_al_citations)
        sleep(1)
    elif args.source == "nyt":
        retriever = NytCitationsCountRetriever(author_first_name=first, author_last_name=last, titles=titles)
        print(f"Nyt citations: {retriever.get_num_citations()}.")
    elif args.source == "hathi":
        retriever = HathiTrustCitationsCountRetriever(
            author_first_name=first,
            author_last_name=last,
            titles=titles,
            verbose=True,
            session=None,
            publication_year=args.publication_year
        )
        print(f"HathiTrust citations: {retriever.get_num_citations()}.")
    else:
        raise RuntimeError(f"Source '{args.source}' not recognized.")


if __name__ == "__main__":
    main()

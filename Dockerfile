FROM python:3.7

# Install cron
RUN apt-get update && apt-get -y install cron
RUN apt-get -y install vim
RUN apt-get -y install less

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Setup cron job
COPY crontab /etc/cron.d/metacanon-cron

RUN chmod 0644 /etc/cron.d/metacanon-cron

RUN crontab /etc/cron.d/metacanon-cron

COPY requirements.txt /code/

WORKDIR /code

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /code/

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log
